package com.planetarium.swing;

import com.planetarium.core.graphics.textures.Texture;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * The class of texture for Swing library.
 */
public class SwingTexture implements Texture, Serializable {
    private static final long serialVersionUID = 1L;

    /** The image for texture. **/
    private Image texture;
    /** The path for this texture. **/
    private String path;

    /** Creates a new texture from the given path of image.
     * @param path The path of image for the texture.
     * @throws IOException If an error occured while reading the stream.  */
    public SwingTexture(String path) throws IOException {
        texture = ImageIO.read(new File(path));
        this.path = path;
    }

    /** Creates a new texture from the given image.
     * @param image The image for this texture.
     * @param path The path for this texture. */
    public SwingTexture(Image image, String path) {
        if (image == null) {
            throw new NullPointerException("The given image is null.");
        }
        this.texture = image;
        this.path = path;

    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public int getWidth() {
        return texture.getWidth(null);
    }

    @Override
    public int getHeight() {
        return texture.getHeight(null);
    }

    @Override
    public Object getContent() {
        return texture;
    }
}
