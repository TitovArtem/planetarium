package com.planetarium.swing;

import com.planetarium.core.graphics.textures.TextureLoader;
import com.planetarium.core.graphics.textures.TexturedObject;

import java.io.IOException;

/**
 * Implements the loader for Swing textures.
 * @see com.planetarium.core.graphics.textures.TextureLoader
 */
public class SwingTextureLoader implements TextureLoader {
    @Override
    public TexturedObject load(TexturedObject object) throws IOException {
        object.setTexture(new SwingTexture(object.getPath()));
        return object;
    }
}
