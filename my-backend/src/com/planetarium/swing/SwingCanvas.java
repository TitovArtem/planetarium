package com.planetarium.swing;

import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.Renderer;
import com.planetarium.core.graphics.scene.Scene;

import javax.swing.*;
import java.io.Serializable;

/**
 * Implements the canvas for renderer based on Swing.
 */
public class SwingCanvas extends JPanel
        implements com.planetarium.core.graphics.Canvas, Serializable {

    private static final long serialVersionUID = 1L;

    /** The scene for drawing. **/
    private Scene scene;
    /** The renderer. It draws the scene. **/
    private Renderer renderer;
    /** The graphics for rendering. **/
    private SwingGraphics graphics;

    /** Creates a new canvas for renderer based on Swing.
     * The renderer is {@link com.planetarium.swing.SwingRenderer}.
     * @param scene The scene which will be rendered.
     * @param width The width for this canvas.
     * @param height The height for this canvas. */
    public SwingCanvas(Scene scene, int width, int height) {
        if (scene == null) {
            throw new NullPointerException("The given scene has null value.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid dimension.");
        }
        this.scene = scene;
        setSize(width, height);
        renderer = new SwingRenderer(this.scene);
        graphics = new SwingGraphics(getWidth(), getHeight());
        renderer.init(graphics);
    }

    /** Creates a new canvas for renderer based on Swing.
     * The renderer is {@link com.planetarium.swing.SwingRenderer}.
     * @param renderer The renderer for rendering the scene..
     * @param width The width for this canvas.
     * @param height The height for this canvas. */
    public SwingCanvas(Renderer renderer, int width, int height) {
        if (renderer == null) {
            throw new NullPointerException("The given scene has null value.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid dimension.");
        }
        scene = renderer.getScene();
        setSize(width, height);
        this.renderer = renderer;
        graphics = new SwingGraphics(getWidth(), getHeight());
        renderer.init(graphics);
    }


    @Override
    public Renderer getRenderer() {
        return renderer;
    }

    @Override
    public void setRenderer(Renderer renderer) {
        if (renderer == null) {
            throw new NullPointerException();
        }
        this.renderer = renderer;
    }

    @Override
    public Graphics getGraphicsObject() {
        return graphics;
    }

    @Override
    public void setGraphicsObject(Graphics graphics) {
        if (graphics == null) {
            throw new NullPointerException("The given graphics is null.");
        }
        this.graphics = (SwingGraphics)graphics;
    }

    @Override
    public void initScene(Scene scene) {
        if (scene == null) {
            throw new NullPointerException("The given scene is null.");
        }

        this.scene = scene;
        renderer.setScene(this.scene);
        renderer.init(graphics);
    }

    @Override
    public void drawScene() {
        renderer.render(graphics);
        repaint();
    }

    @Override
    public void paint(java.awt.Graphics g) {
        super.paint(g);
        g.drawImage(graphics.get(), 0, 0, this);

    }

}
