package com.planetarium.swing;

import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.GraphicsType;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * Implements the graphics for the renderer which is based on Swing.
 */
public class SwingGraphics implements Graphics<BufferedImage>, Serializable {
    private static final long serialVersionUID = 1L;

    /** The content of this graphics. **/
    private BufferedImage image;

    /** Creates a new graphics for the renderer based on Swing.
     * @param image The image for graphics. */
    public SwingGraphics(BufferedImage image) {
        if (image == null) {
            throw new NullPointerException("The given image has null value.");
        }

        this.image = image;
    }

    /** Creates a new graphics for the renderer based on Swing.
     * The image type for BufferedImage is {@link BufferedImage#TYPE_INT_ARGB}
     * @param width The width of this graphics.
     * @param height The height of this graphics. */
    public SwingGraphics(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid dimension.");
        }
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    }

    @Override
    public void set(BufferedImage obj) {
        if (image == null) {
            throw new NullPointerException("The given image is null.");
        }

        image = obj;
    }

    @Override
    public BufferedImage get() {
        return image;
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public GraphicsType getType() {
        return GraphicsType.SWING;
    }
}
