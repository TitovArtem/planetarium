package com.planetarium.swing;

import com.planetarium.core.graphics.Configs;
import com.planetarium.core.graphics.Dimension;
import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.Renderer;
import com.planetarium.core.graphics.scene.Camera;
import com.planetarium.core.graphics.scene.Scene;
import com.planetarium.core.graphics.textures.TexturedSphere;
import com.planetarium.core.math.*;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.math.geom.Triangle;
import com.planetarium.core.math.transforms.TransformMatrix;
import com.planetarium.core.objects.Light;
import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceSystem;
import com.planetarium.core.objects.wireframe.WireframeObject;
import com.planetarium.core.objects.wireframe.WireframeSphere;
import com.planetarium.core.objects.wireframe.WireframeSphereFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Implements the renderer based on Swing library.
 */
public class SwingRenderer extends Renderer implements Serializable {

    private static final int MERIDIANS_NUM = 40;
    private static final int PARALLELS_NUM = 40;
    /** The array of wire-frame objects for this scene. **/
    private ArrayList<WireframeSphere> objects = new ArrayList<>();
    /** The z-buffer for this scene. **/
    private float zBuffer[][];
    private Configs configs;
    private boolean isLight;
    private int width = 0, height = 0;

    public SwingRenderer(Scene scene) {
        if (scene == null) {
            throw new NullPointerException("The given scene is null.");
        }
        this.scene = scene;
        configs = new Configs();
    }

    public SwingRenderer(Scene scene, Configs configs) {
        if (scene == null) {
            throw new NullPointerException("The given scene is null.");
        } else if (configs == null) {
            throw new NullPointerException("The given configs is null");
        }
        this.scene = scene;
        this.configs = configs;
    }

    private WireframeSphere createSphere(TexturedSphere sphere) {
        Dimension textureDimension = new Dimension(sphere.getTexture().getWidth(),
                sphere.getTexture().getHeight());

        ScaleMachine scaleMachine = scene.getScaleMachine();
        Sphere scaledSphere = (Sphere)scaleMachine.scale(sphere);
        scaledSphere.setCenter(new Vector3());

        WireframeSphereFactory factory = new WireframeSphereFactory(scaledSphere,
                textureDimension);

        factory.setNumMeridians(MERIDIANS_NUM);
        factory.setNumParallels(PARALLELS_NUM);

        WireframeObject wireframeObject = factory.create();

        WireframeSphere wireframeSphere =
                new WireframeSphere(wireframeObject, sphere.getCenter(), 20f);

        return wireframeSphere;
    }

    private void initObjects() {
        objects = new ArrayList<>();
        SpaceSystem system = scene.getSystem();
        for (int i = 0; i < system.getSize(); i++) {
            objects.add(createSphere((TexturedSphere) system.getObject(i).getShape()));
        }
    }

    private void initZbuffer() {
        for (int i = 0; i < zBuffer.length; i++) {
            for (int j = 0; j < zBuffer[0].length; j++) {
                zBuffer[i][j] = Float.MIN_VALUE;
            }
        }
    }

    @Override
    public void init(Graphics graphics) {
        zBuffer = new float[graphics.getWidth()][graphics.getHeight()];
        initObjects();
        float factor = foundScaleFactor(objects);
        for (WireframeSphere sphere : objects) {
            normalize(sphere, factor);
            sphere.calculateNormals(MERIDIANS_NUM, PARALLELS_NUM);
        }
    }

    public boolean valid(Triangle triangle) {
        Vector3 a = triangle.getA().getPosition();
        Vector3 b = triangle.getB().getPosition();
        Vector3 c = triangle.getC().getPosition();

        boolean isValid = true;

        int min = -10;
        int width = this.width + 10;
        int height = this.height + 10;

        if (a.x < min || b.x < min || c.x < min) {
            isValid = false;
        } else if (a.y < min || b.y < min || c.y < min) {
            isValid = false;
        } else if (a.x > width || b.x > width || c.x > width) {
            isValid = false;
        } else if (a.y > height || b.y > height || c.y > height) {
            isValid = false;
        }


        return isValid;
    }

    public void drawTriangle(BufferedImage image, BufferedImage texture, Triangle itriangle, Vector3 light) {
        if (!valid(itriangle)) {
            return;
        }

        Triangle triangle = itriangle.copy();
        triangle.sortVertexes();

        int ya = (int) triangle.getA().getPosition().y;
        int yb = (int) triangle.getB().getPosition().y;
        int yc = (int) triangle.getC().getPosition().y;
        Vector3 t0 = triangle.getA().getPosition();
        Vector3 t1 = triangle.getB().getPosition();
        Vector3 t2 = triangle.getC().getPosition();
        Vector2i uvt0 = triangle.getA().getTexel();
        Vector2i uvt1 = triangle.getB().getTexel();
        Vector2i uvt2 = triangle.getC().getTexel();
        float it0 = triangle.getA().getNormal().dot(light);
        float it1 = triangle.getB().getNormal().dot(light);
        float it2 = triangle.getC().getNormal().dot(light);

        int totalHeight = yc - ya;
        for (int i = 0; i < totalHeight; i++) {
            boolean secondHalf = i > yb - ya || yb == ya;
            int segmentHeight = secondHalf ? yc - yb : yb - ya;
            float alpha = (float) i / totalHeight;
            float beta = (float) (i - (secondHalf ? yb - ya : 0)) / segmentHeight;
            Vector3 a = t0.copy().add((t2.copy().sub(t0)).multiple(alpha));
            Vector3 b = secondHalf ? t1.copy().add((t2.copy().sub(t1)).multiple(beta)) :
                    t0.copy().add((t1.copy().sub(t0)).multiple(beta));

            Vector2i uva = uvt0.copy().add((uvt2.copy().sub(uvt0)).multiple(alpha));
            Vector2i uvb = secondHalf ? uvt1.copy().add((uvt2.copy().sub(uvt1)).multiple(beta)) :
                    uvt0.copy().add((uvt1.copy().sub(uvt0)).multiple(beta));

            float ita = it0 + (it2 - it0) * alpha;
            float itb = secondHalf ? it1 + (it2 - it1) * beta : it0 + (it1 - it0) * beta;

            if (a.x > b.x) {
                Vector3 temp = a.copy();
                a.set(b);
                b.set(temp);
                Vector2i temp2 = uva.copy();
                uva.set(uvb);
                uvb.set(temp2);
                float temp3 = ita;
                ita = itb;
                itb = temp3;
            }
            int ax = (int) a.x;
            int bx = (int) b.x;
            for (int j = ax; j <= bx; j++) {
                float phi = ax == bx ? 1.f : (float) (j - ax) / (float) (bx - ax);
                Vector3 p = a.copy().add((b.copy().sub(a)).multiple(phi));
                Vector2i uvp = uva.copy().add((uvb.copy().sub(uva)).multiple(phi));
                float itp = ita + (itb - ita) * phi;
                int x = (int) p.x;
                int y = (int) p.y;

                if (x >= width || y >= height || x < 0 || y < 0)
                    continue;

                if (zBuffer[x][y] < p.z) {
                    zBuffer[x][y] = p.z;

                    if (isLight) {
                        itp = 1f;
                    } else {
                        if (itp < 0) itp = 0f;
                        itp = scene.getLightModel().getIntensity(itp);
                    }

                    Color color = new Color(texture.getRGB(uvp.x, uvp.y));
                    int red = (int) (color.getRed() * itp);
                    int green = (int) (color.getGreen() * itp);
                    int blue = (int) (color.getBlue() * itp);
                    image.setRGB(x, y, new Color(red, green, blue).getRGB());
                }
            }
        }
    }

    private void convertObject(WireframeSphere sphere, SpaceObject spaceObject) {
        Camera camera = scene.getCurrentCamera();

        ScaleMachine scaleMachine = scene.getScaleMachine();
        Sphere shape3D = (Sphere)scaleMachine.scale(spaceObject.getShape());

        float rotationAngle = spaceObject.getAxisData().getRotationAngle();
        float inclination = spaceObject.getAxisData().getAxialTilt();

        // Create the model matrix
        Matrix4 rotationY = TransformMatrix.createRotateMatrix(
                rotationAngle * FloatMath.DEGREE_TO_RADIAN, 0f, 0f, 1f);
        Matrix4 rotationX1 = TransformMatrix.createRotateMatrix(
                inclination * FloatMath.DEGREE_TO_RADIAN, 0f, 0f, 1f);
        Matrix4 rotationX2 = TransformMatrix.createRotateMatrix(
                inclination * FloatMath.DEGREE_TO_RADIAN, 0f, 1f, 0f);
        Matrix4 rotationX3 = TransformMatrix.createRotateMatrix(
                inclination * FloatMath.DEGREE_TO_RADIAN, 1f, 0f, 0f);
        Matrix4 rotation = rotationX1.multiple(rotationX2).
                multiple(rotationX3).multiple(rotationY);


        Matrix4 move = TransformMatrix.createMoveMatrix(shape3D.getCenter());
        Matrix4 model = move.multiple(rotation);

        Matrix4 projection = camera.getProjection().copy();
        Matrix4 view = camera.getView();
        Matrix4 transform = projection.multiple(view).multiple(model);
        Matrix4 viewport = camera.getViewport();

        Vertex[] vertexes = sphere.getVertexes();
        for (int i = 0; i < vertexes.length; i++) {
            Vector4 vector4 = new Vector4(vertexes[i].getPosition());
            Vector4 normal4 = new Vector4(vertexes[i].getNormal());
            transform.multiple(vector4);

            rotation.multiple(normal4);

            sphere.getVertex(i).getNormal().set(normal4.x, normal4.y, normal4.z);
            float w = vector4.w;
            vector4.set(vector4.x / w, vector4.y / w, vector4.z / w, 1);

            viewport.multiple(vector4);
            vertexes[i].getPosition().set(vector4.x, vector4.y, vector4.z);
        }
    }

    private float getMaxAbsoluteValue(Vector3 vector) {
        float max = FloatMath.abs(vector.x);
        if (FloatMath.abs(vector.y) > max)  {
            max = FloatMath.abs(vector.y);
        }
        if (FloatMath.abs(vector.z) > max) {
            max = FloatMath.abs(vector.z);
        }

        return max;
    }

    private float foundScaleFactor(ArrayList<WireframeSphere> spheres) {
        float maxValue = 0f;
        for (int i = 0; i < spheres.size(); i++) {
            Vector3[] vectors = spheres.get(i).getPositions();
            for (int j = 0; j < vectors.length; j++) {
                float curValue = getMaxAbsoluteValue(vectors[j]);
                if (curValue > maxValue) {
                    maxValue = curValue;
                }
            }
        }

        return maxValue;
    }

    private void normalize(WireframeSphere spheres, float factor) {

        Vector3[] vectors = spheres.getPositions();
        for (int j = 0; j < vectors.length; j++) {
                vectors[j].div(factor);
        }
    }

    private Vertex[] getObjectState(WireframeObject object) {
        Vertex[] vertexes = object.getVertexes();
        Vertex[] copies = new Vertex[vertexes.length];

        for (int i = 0; i < vertexes.length; i++) {
            copies[i] = new Vertex(vertexes[i]);
        }
        return copies;
    }

    private void setObjectState(WireframeObject object, Vertex[] state) {
        Vertex[] vertexes = object.getVertexes();

        for (int i = 0; i < vertexes.length; i++) {
            vertexes[i].set(state[i]);
        }
    }

    private Color convetColorToSwing(com.planetarium.core.graphics.Color color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue());
    }

    private void drawCaption(BufferedImage image, SpaceObject object,
                             WireframeSphere sphere, int index) {
        String name = object.getInfo().get("Имя");
        if (name != null) {
            java.awt.Graphics g = image.createGraphics();
            if (index == configs.getSelectedObjectIndex()) {
                g.setColor(convetColorToSwing(configs.getSelectedCaptionColor()));
            } else {
                g.setColor(convetColorToSwing(configs.getCaptionColor()));
            }

            g.drawString(name, (int) sphere.getVertex(0).getPosition().x + 5,
                    (int) sphere.getVertex(0).getPosition().y + 5);
        }
    }

    private void drawObject(BufferedImage image,  int i) {
        isLight = false;
        SpaceObject spaceObject = scene.getSystem().getObject(i);
        if (spaceObject instanceof Light) {
            isLight = true;
        }


        WireframeSphere sphere = objects.get(i);
        Vertex[] state = getObjectState(sphere);

        convertObject(sphere, spaceObject);

        Vector3 lightVector = scene.getLight(0).getCenter().copy();
        lightVector.sub(spaceObject.getShape().getCenter());
        lightVector.normalize();

        BufferedImage texture = (BufferedImage)
                ((TexturedSphere)spaceObject.getShape()).getTexture().getContent();

        Triangle[] triangles = sphere.getTriangles();
        for (int j = 0; j < triangles.length; j++) {
            drawTriangle(image, texture, triangles[j], lightVector);
        }

        if (configs.isCaptions()) {
            drawCaption(image, spaceObject, sphere, i);
        }

        setObjectState(sphere, state);
    }

    public void drawBackground(BufferedImage image) {
        java.awt.Graphics g = image.getGraphics();
        if (scene.getBackgroundTexture() == null) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, image.getWidth(), image.getHeight());
        } else {
            Image texture = (Image) scene.getBackgroundTexture().getContent();
            g.drawImage(texture, 0, 0, null, null);
        }
    }


    @Override
    public void render(Graphics graphics) {
        if (zBuffer == null) {
            throw new NullPointerException("The renderer wasn't initialized.");
        }
        initZbuffer();

        width = graphics.getWidth();
        height = graphics.getHeight();

        BufferedImage image = (BufferedImage)graphics.get();
        drawBackground(image);

        Camera camera = scene.getCurrentCamera();
        float widthHeightRatio = (float) width / (float) height;
        camera.calculatePerspective(20, widthHeightRatio, 1f, 1000f);
        camera.calculateView();
        camera.calculateViewport(width / 8, height / 8,
                width * 3 / 4, height * 3 / 4);


        SpaceSystem system = scene.getSystem();

        for (int i = 0; i < system.getSize(); i++) {
            drawObject(image, i);
        }
    }
}
