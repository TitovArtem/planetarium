import com.planetarium.core.graphics.Canvas;
import com.planetarium.core.graphics.Configs;
import com.planetarium.core.graphics.scene.Camera;
import com.planetarium.core.graphics.scene.ObjectCenterScaler;
import com.planetarium.core.graphics.scene.Scene;
import com.planetarium.core.loader.PlanetSystemLoader;
import com.planetarium.core.loader.SystemLoader;
import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.objects.Light;
import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceSystem;
import com.planetarium.core.physics.SystemEngine;
import com.planetarium.jogl.JoglCanvas;
import com.planetarium.jogl.JoglRenderer;
import com.planetarium.swing.SwingCanvas;
import com.planetarium.swing.SwingRenderer;
import com.planetarium.swing.SwingTexture;
import com.planetarium.swing.SwingTextureLoader;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class PlanetariumViewer {
    public static final int SWING = 0;
    public static final int JOGL = 0;
    private JPanel mainPanel;
    private JPanel centerPanel;
    private JPanel westPanel;
    private JPanel eastPanel;
    private JPanel objectsSelectorPanel;
    private JLabel comboboxLabel;
    private JComboBox objectsComboBox;
    private JCheckBox isCaptionCheckedBox;
    private JCheckBox realSizeCheckedBox;
    private JLabel textAreaDescription;
    private JTextPane descriptionTextPane;
    private JScrollPane scrollPane;
    private JSlider speedSlider;
    private JFrame frame;
    private Scene scene;
    private Canvas canvas;
    private SystemEngine engine;
    private Configs configs;
    private EngineThread engineThread;
    private Color bgComponents = Color.DARK_GRAY;
    private int canvasWidth = 700;
    private int canvasHeight = 500;
    private int runLibraryMode = SWING;

    private String planetSystemPath = "/home/artem-bars/temp/course_work/" +
            "planetarium/planetarium-viewer/src/planetsytems/solarsystem/solarsystem.system";

    private String bgTexturePath = "/home/artem-bars/temp/course_work/" +
            "planetarium/planetarium-viewer/src/planetsytems/solarsystem/milkyway.jpg";

    public PlanetariumViewer() {
        frame = new JFrame("Planetarium Viewer");

        $$$setupUI$$$();

        frame.setSize(920, 520);
        frame.setMaximumSize(new Dimension(920, 520));
        frame.setResizable(false);
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(bgComponents);

        configs = new Configs();

        try {
            initSpaceSystem();
        } catch (IOException exc) {
            loadSystemError();
            System.err.print(exc);
            System.exit(1);
        }

        try {
            initSwingCanvas();
            //initJoglCanvas();
        } catch (IOException exc) {
            loadTexturesError();
            System.exit(2);
        }

        centerPanel.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
        initListeners();
        eastPanel.setBackground(bgComponents);
        frame.add($$$getRootComponent$$$());
        frame.setVisible(true);

        canvas.drawScene();
        engineThread.start();

        fillComboboxItem();
        objectsComboBox.setSelectedIndex(1);
        createMenuBar();
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (Exception e) {
            System.out.println("> Fatal error: Invalid upload look and feel theme.");
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PlanetariumViewer();
            }
        });
    }

    private void loadSystemError() {
        JOptionPane.showMessageDialog(mainPanel, "Не удалось считать систему. " +
                        "Проверьте правильность \nи наличие файлов системы, и пропробуйте заново.",
                "Ошибка считывания системы", JOptionPane.ERROR_MESSAGE);
    }

    private void loadTexturesError() {
        JOptionPane.showMessageDialog(mainPanel, "Не удалось загрузить текстуры. " +
                        "\nПроверьте наличие изображений текстур, и пропробуйте заново.",
                "Ошибка считывания текстур", JOptionPane.ERROR_MESSAGE);
    }

    private void loadNewSystem() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(mainPanel);

        File file = fileChooser.getSelectedFile();
        SpaceSystem spaceSystem;
        try {
            SystemLoader loader = new PlanetSystemLoader(file.getAbsolutePath());
            spaceSystem = loader.load();
        } catch (IOException exc) {
            loadSystemError();
            System.err.print(exc);
            return;
        }

        Scene tempScene = new Scene(spaceSystem);
        try {
            tempScene.loadTextures(new SwingTextureLoader());
        } catch (IOException exc) {
            loadTexturesError();
            System.err.print(exc);
            return;
        }

        scene.setSystem(tempScene.getSystem());
        scene.setScaleMachine(new SolarSystemScaleMachine(tempScene.getSystem()));
        engine.init(tempScene.getSystem());
        canvas.initScene(scene);
        engine.move(0);
        fillComboboxItem();
    }

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Файл");
        final JMenuItem loadFileItem = new JMenuItem("Загрузить систему");
        fileMenu.add(loadFileItem);
        loadFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadNewSystem();
            }
        });
        fileMenu.addSeparator();
        JMenuItem exitItem = new JMenuItem("Выход");
        fileMenu.add(exitItem);
        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        JMenu helpMenu = new JMenu("Справка");
        JMenuItem aboutHelpItem = new JMenuItem("О проекте");
        aboutHelpItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(mainPanel, "Программа для моделирования движения " +
                        "различных планетных систем.\n Разработана в рамках курсового проекта " +
                        "по курсу 'Компьютерная графика'.\n Автор: Титов Артем ИУ7-53.\n " +
                        "Научный руководитель: Барышникова Марина Юрьевна. \n");
            }
        });
        helpMenu.add(aboutHelpItem);
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        fileMenu.setBackground(bgComponents);
        helpMenu.setBackground(bgComponents);
        menuBar.setOpaque(true);
        menuBar.setBackground(bgComponents);

        frame.setJMenuBar(menuBar);
    }

    private void initListeners() {

        speedSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                float value = speedSlider.getValue();
                value = FloatMath.pow(value / 20f, 3);
                engineThread.setDays(value);
            }
        });

        objectsComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try {
                    int index = objectsComboBox.getSelectedIndex();
                    configs.setSelectedObjectIndex(index);
                    SpaceObject object = scene.getSystem().getObject(index);
                    String str = object.getInfo().get("Описание");
                    if (str == null) {
                        str = "";
                    }
                    descriptionTextPane.setText(str);
                } catch (ArrayIndexOutOfBoundsException exc) {
                    System.out.println(exc);
                }
            }
        });

        isCaptionCheckedBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean isSelected = isCaptionCheckedBox.isSelected();
                configs.setIsCaptions(isSelected);
            }
        });

        realSizeCheckedBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean isSelected = realSizeCheckedBox.isSelected();
                if (isSelected) {
                    scene.setScaleMachine(new ObjectCenterScaler(10, 10, 10));
                    canvas.initScene(scene);
                } else {
                    scene.setScaleMachine(new SolarSystemScaleMachine(
                            scene.getSystem()));
                    canvas.initScene(scene);
                }
            }
        });
    }

    private void initSwingCanvas() throws IOException {
        scene.loadTextures(new SwingTextureLoader());
        BufferedImage texture = ImageIO.read(new File(bgTexturePath));
        Image image = texture.getScaledInstance(canvasWidth, canvasHeight, Image.SCALE_FAST);
        scene.setBackgroundTexture(new SwingTexture(image, bgTexturePath));


        JPanel canvas = new SwingCanvas(new SwingRenderer(scene, configs),
                canvasWidth, canvasHeight);
        this.canvas = (Canvas) canvas;

        engineThread = new EngineThread(this.canvas, engine);
        CameraController cameraController =
                new CameraController(scene.getCurrentCamera(), engineThread);
        canvas.addMouseWheelListener(cameraController);
        canvas.addMouseMotionListener(cameraController);
        canvas.addMouseListener(cameraController);
        centerPanel.add(canvas, BorderLayout.CENTER);
    }

    private void initJoglCanvas() throws IOException {
        java.awt.Canvas canvas = new JoglCanvas(
                new JoglRenderer(scene), canvasWidth, canvasHeight, bgTexturePath);
        this.canvas = (Canvas) canvas;
        engineThread = new EngineThread(this.canvas, engine);
        CameraController cameraController =
                new CameraController(scene.getCurrentCamera(), engineThread);
        canvas.addMouseWheelListener(cameraController);
        canvas.addMouseMotionListener(cameraController);
        canvas.addMouseListener(cameraController);
        frame.getContentPane().add(canvas, BorderLayout.CENTER);
    }

    private void fillComboboxItem() {
        int index = 1;
        objectsComboBox.removeAllItems();
        SpaceSystem spaceSystem = scene.getSystem();
        for (int i = 0; i < spaceSystem.getSize(); i++) {
            String name = spaceSystem.getObject(i).getInfo().get("Имя");
            if (name == null) {
                objectsComboBox.addItem("Объект #" + index++);
            } else {
                objectsComboBox.addItem(name);
            }

        }
    }

    private void initSpaceSystem() throws IOException {
        SpaceSystem spaceSystem;
        SystemLoader loader = new PlanetSystemLoader(planetSystemPath);
        spaceSystem = loader.load();

        scene = new Scene(spaceSystem);
        scene.addLight((Light) spaceSystem.getObject(0));
        Camera camera = new Camera(
                new Vector3(0f, 0f, -5f),
                new Vector3(0, 0, 0),
                new Vector3(0, 1, 0));

        scene.addCamera(camera);
        scene.setCurrentCamera(0);
        scene.setScaleMachine(new SolarSystemScaleMachine(spaceSystem));
        engine = new SystemEngine(spaceSystem);
        engine.move(0);
    }

    private void createUIComponents() {
        westPanel = new JPanel();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout(0, 0));
        centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout(0, 0));
        centerPanel.setPreferredSize(new Dimension(700, 500));
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        westPanel.setForeground(new Color(-12895429));
        westPanel.setMaximumSize(new Dimension(511, 120));
        westPanel.setPreferredSize(new Dimension(900, 50));
        westPanel.setVerifyInputWhenFocusTarget(false);
        westPanel.setVisible(false);
        mainPanel.add(westPanel, BorderLayout.NORTH);
        eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout(0, 0));
        eastPanel.setPreferredSize(new Dimension(230, 26));
        mainPanel.add(eastPanel, BorderLayout.EAST);
        objectsSelectorPanel = new JPanel();
        objectsSelectorPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        objectsSelectorPanel.setPreferredSize(new Dimension(450, 190));
        eastPanel.add(objectsSelectorPanel, BorderLayout.CENTER);
        comboboxLabel = new JLabel();
        comboboxLabel.setPreferredSize(new Dimension(130, 20));
        comboboxLabel.setText("Выбрать объект:");
        objectsSelectorPanel.add(comboboxLabel);
        objectsComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        objectsComboBox.setModel(defaultComboBoxModel1);
        objectsComboBox.setPreferredSize(new Dimension(170, 26));
        objectsComboBox.setRequestFocusEnabled(false);
        objectsSelectorPanel.add(objectsComboBox);
        textAreaDescription = new JLabel();
        textAreaDescription.setAutoscrolls(false);
        textAreaDescription.setPreferredSize(new Dimension(80, 20));
        textAreaDescription.setText("Описание:");
        objectsSelectorPanel.add(textAreaDescription);
        scrollPane = new JScrollPane();
        scrollPane.setMaximumSize(new Dimension(180, 200));
        scrollPane.setPreferredSize(new Dimension(180, 200));
        objectsSelectorPanel.add(scrollPane);
        descriptionTextPane = new JTextPane();
        descriptionTextPane.setEditable(false);
        descriptionTextPane.setMaximumSize(new Dimension(170, 200));
        descriptionTextPane.setMinimumSize(new Dimension(170, 200));
        descriptionTextPane.setPreferredSize(new Dimension(170, 200));
        scrollPane.setViewportView(descriptionTextPane);
        realSizeCheckedBox = new JCheckBox();
        realSizeCheckedBox.setMinimumSize(new Dimension(163, 34));
        realSizeCheckedBox.setPreferredSize(new Dimension(180, 24));
        realSizeCheckedBox.setText("Реальные пропорции");
        objectsSelectorPanel.add(realSizeCheckedBox);
        isCaptionCheckedBox = new JCheckBox();
        isCaptionCheckedBox.setPreferredSize(new Dimension(180, 34));
        isCaptionCheckedBox.setSelected(true);
        isCaptionCheckedBox.setText("Отображать названия");
        objectsSelectorPanel.add(isCaptionCheckedBox);
        final JLabel label1 = new JLabel();
        label1.setMinimumSize(new Dimension(180, 16));
        label1.setPreferredSize(new Dimension(180, 15));
        label1.setText("");
        objectsSelectorPanel.add(label1);
        final JLabel label2 = new JLabel();
        label2.setPreferredSize(new Dimension(190, 16));
        label2.setRequestFocusEnabled(false);
        label2.setText("Скорость движения планет:");
        objectsSelectorPanel.add(label2);
        speedSlider = new JSlider();
        speedSlider.setMaximum(50);
        speedSlider.setMinimum(-50);
        speedSlider.setPreferredSize(new Dimension(170, 16));
        speedSlider.setValue(0);
        speedSlider.setVisible(true);
        objectsSelectorPanel.add(speedSlider);
        final JLabel label3 = new JLabel();
        label3.setPreferredSize(new Dimension(130, 16));
        label3.setRequestFocusEnabled(true);
        label3.setText("Назад");
        objectsSelectorPanel.add(label3);
        final JLabel label4 = new JLabel();
        label4.setPreferredSize(new Dimension(50, 16));
        label4.setText("Вперед");
        objectsSelectorPanel.add(label4);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
