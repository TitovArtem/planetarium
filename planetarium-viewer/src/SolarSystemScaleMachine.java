import com.planetarium.core.math.ScaleMachine;
import com.planetarium.core.math.geom.Shape3D;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.objects.SpaceSystem;

/**
 * Scale machine for solar system.
 */
public class SolarSystemScaleMachine implements ScaleMachine {
    /** The space system. **/
    private SpaceSystem spaceSystem;

    /** Constructs a new scale machine for the Solar System.
     * @param spaceSystem A space system of the Solar System. */
    public SolarSystemScaleMachine(SpaceSystem spaceSystem) {
        this.spaceSystem = spaceSystem;
    }

    @Override
    public Shape3D scale(Shape3D shape) {
        if (shape instanceof Sphere) {
            Sphere sphere = ((Sphere) shape);
            Sphere result = sphere.copy();
            if (sphere == spaceSystem.getObject(0).getShape()) {
                result.setRadius(sphere.getRadius() / 18);
            } else if (sphere == spaceSystem.getObject(1).getShape()) {
                result.getCenter().multiple(150);
                result.setRadius(sphere.getRadius() * 2.3f);
            } else if (sphere == spaceSystem.getObject(2).getShape()) {
                result.getCenter().multiple(110);
                result.setRadius(sphere.getRadius() * 2f);
            } else if (sphere == spaceSystem.getObject(3).getShape()) {
                result.getCenter().multiple(110);
                result.setRadius(sphere.getRadius() * 2f);
            } else if (sphere == spaceSystem.getObject(4).getShape()) {
                result.getCenter().multiple(100);
                result.setRadius(sphere.getRadius() * 2.3f);
            } else if (sphere == spaceSystem.getObject(5).getShape()) {
                result.getCenter().multiple(63);
                result.setRadius(sphere.getRadius() * 8f);
            } else if (sphere == spaceSystem.getObject(6).getShape()) {
                result.getCenter().multiple(40);
                result.setRadius(sphere.getRadius() / 3f);
            } else if (sphere == spaceSystem.getObject(7).getShape()) {
                result.getCenter().multiple(28);
                result.setRadius(sphere.getRadius() / 3f);
            } else if (sphere == spaceSystem.getObject(8).getShape()) {
                result.getCenter().multiple(16);
                result.setRadius(sphere.getRadius() / 2f);
            } else if (sphere == spaceSystem.getObject(9).getShape()) {
                result.getCenter().multiple(12);
                result.setRadius(sphere.getRadius() / 2f);
            } else if (sphere == spaceSystem.getObject(10).getShape()) {
                result.getCenter().multiple(10);
                result.setRadius(sphere.getRadius() * 3f);
            }
            result.setRadius(result.getRadius() / 2);
            result.setCenter(result.getCenter().div(30));
            return result;
        }
        return shape;
    }
}
