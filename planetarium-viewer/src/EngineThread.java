import com.planetarium.core.graphics.Canvas;
import com.planetarium.core.physics.SystemEngine;

/**
 * The thread for the system engine.
 */
public class EngineThread extends Thread {

    private Canvas canvas;
    private SystemEngine engine;
    private float days;

    public EngineThread(Canvas canvas, SystemEngine engine) {
        this.canvas = canvas;
        this.engine = engine;
        days = 0;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public SystemEngine getEngine() {
        return engine;
    }

    public void setEngine(SystemEngine engine) {
        this.engine = engine;
    }

    public float getDays() {
        return days;
    }

    public void setDays(float days) {
        this.days = days;
    }

    @Override
    public void run() {
        while (true) {
            try {
                canvas.drawScene();
            } catch (IndexOutOfBoundsException exc) {
                System.out.print(exc);
            } catch (NullPointerException exc) {
                System.out.print(exc);
            }

            try {
                Thread.sleep(7);
            } catch (InterruptedException exc) {
                System.err.print(exc);
            }

            engine.move(days);
        }

    }
}
