import com.planetarium.core.graphics.scene.Camera;
import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Matrix4;
import com.planetarium.core.math.Vector2i;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.transforms.TransformMatrix;
import com.planetarium.core.math.transforms.Transformer;

import java.awt.event.*;

/**
 * Implements all mouse listeners to work with the camera.
 */
public class CameraController implements MouseMotionListener, MouseWheelListener, MouseListener {
    final int WHEEL_STEP_FACTOR = 2;
    private Camera camera;
    private Vector2i point;
    private EngineThread engineThread;
    private float maxZ = -3f;

    /** Creates a new controller for the camera.
     * @param camera The camera. */
    public CameraController(Camera camera, EngineThread thread) {
        if (camera == null) {
            throw new NullPointerException("The given camera has null value.");
        } else if (thread == null) {
            throw new NullPointerException("The given engine thread is null");
        }

        this.camera = camera;
        this.point = new Vector2i(0, 0);
        this.engineThread = thread;
    }

    /** @return The camera of this controller for changing. **/
    public Camera getCamera() {
        return camera;
    }

    /** Sets the camera for this controller.
     * @param camera The camera for this controller. **/
    public void setCamera(Camera camera) {
        if (camera == null) {
            throw new NullPointerException("The given camera is null.");
        }
        this.camera = camera;
    }


    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {
        point.x = e.getX();
        point.y = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {
        int curX = e.getX();
        int curY = e.getY();

        float angleX = (float)(curX - point.x) / 150 * FloatMath.DEGREE_TO_RADIAN;
        float angleY = (float)(curY - point.y) / 150 * FloatMath.DEGREE_TO_RADIAN;

        System.out.println("angles: " + angleX + " " + angleY);

        //Matrix4 rotateX = TransformMatrix.createRotateMatrix(angleX, 0f, 1f, 0f);
        Matrix4 rotateY = TransformMatrix.createRotateMatrix(angleY, 1f, 0f, 0f);
        Vector3 temp = camera.getPosition().copy();
        Transformer.transform(temp, rotateY);
        if (temp.z < maxZ) {
            Transformer.transform(camera.getPosition(), rotateY);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        Vector3 pos = camera.getPosition();
        float val = e.getWheelRotation() * WHEEL_STEP_FACTOR;
        if (pos.z + val < maxZ) {
            pos.set(pos.x, pos.y, pos.z + val);
        }
        System.out.println(pos);
        /*
        Vector3 pos = new Vector3(
                camera.getPosition().x,
                camera.getPosition().y,
                camera.getPosition().z
        );
        float val = e.getWheelRotation() * WHEEL_STEP_FACTOR;
        if (pos.z + val < maxZ) {
            pos.set(pos.x, pos.y, pos.z + val);
        }
        System.out.println(pos);*/
    }
}
