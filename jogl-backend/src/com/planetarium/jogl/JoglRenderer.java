package com.planetarium.jogl;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.Renderer;
import com.planetarium.core.graphics.scene.Camera;
import com.planetarium.core.graphics.scene.Scene;
import com.planetarium.core.graphics.textures.TexturedObject;
import com.planetarium.core.graphics.textures.TexturedSphere;
import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.geom.Shape3D;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.objects.Light;
import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceSystem;

import java.io.Serializable;

/**
 * Implements the simple renderer for rendering the space system.
 * It renders the only {@link com.planetarium.core.graphics.textures.TexturedSphere} objects.
 * This render uses the {@link com.jogamp.opengl.GL2} object for rendering.
 */
public class JoglRenderer extends Renderer implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Creates a new renderer.
     * @param scene The scene which will be rendered. */
    public JoglRenderer(Scene scene) {
        if (scene == null) {
            throw new NullPointerException("The given scene has null value.");
        }

        this.scene = scene;
    }

    /* Prepares the parameters of light. */
    private void initLightProperties(GL2 gl) {

        float SHINE_ALL_DIRECTIONS = 1;
        
        Light light;
        try {
            light = scene.getLight(0);
        } catch (IndexOutOfBoundsException exc) {
            throw new IndexOutOfBoundsException("The scene hasn't " +
                    "sources of lights.");
        }
        
        Vector3 lightCenter = light.getCenter().copy();
        float[] lightPos = {lightCenter.x, lightCenter.y, lightCenter.z, SHINE_ALL_DIRECTIONS};
        float[] lightColorAmbient = {0.3f, 0.3f, 0.3f, 1f};
        float[] lightColorSpecular = {0.8f, 0.8f, 0.8f, 1f};

        // Set light parameters.
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightColorAmbient, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightColorSpecular, 0);

        // Enable lighting in GL.
        gl.glEnable(GL2.GL_LIGHT1);
        gl.glEnable(GL2.GL_LIGHTING);
    }

    /* Prepares the parameters of material. */
    public void initMaterialProperties(GL2 gl) {
        float[] rgba = {1f, 1f, 1f};
        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_AMBIENT, rgba, 0);
        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_SPECULAR, rgba, 0);
        gl.glMaterialf(GL.GL_FRONT, GL2.GL_SHININESS, 0.5f);
    }
    
    /* Prepares the parameters of the current camera. */
    public void initCameraProperties(GL2 gl, GLU glu, int width, int height) {
        
        // Change to projection matrix.
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        Camera camera = scene.getCurrentCamera();
        
        // Perspective.
        float widthHeightRatio = (float) width / (float) height;

        glu.gluPerspective(45, widthHeightRatio, 1, 1000);

        Vector3 center = camera.getDirection();
        Vector3 eye = camera.getPosition();
        Vector3 up = camera.getUp();

        glu.gluLookAt(
                eye.x, eye.y, eye.z,
                center.x, center.y, center.z,
                up.x, up.y, up.z
        );

        // Change back to model view matrix.
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    private void drawCircle(GL2 gl, float cx, float cy, float radius) {
        gl.glBegin(GL2.GL_LINE_LOOP);
        gl.glColor3i(100, 100, 255);
        int num_segments = 50;
        for(int i = 0; i < num_segments; i++)
        {
            float theta = 2.0f * 3.1415926f * (float) i/ (float) num_segments;//get the current angle

            float x = radius * FloatMath.cos(theta);//calculate the x component
            float y = radius * FloatMath.sin(theta);//calculate the y component

            gl.glVertex3f(x + cx, y + cy, 0f);//output vertex

        }
        gl.glEnd();
    }

    private void drawPlanet(GL2 gl, GLU glu, SpaceObject object) {
        if (!(object.getShape() instanceof TexturedSphere)) {
            return;
        }

        Shape3D shape = scene.getScaleMachine().scale(object.getShape());

        Vector3 center = shape.getCenter().copy();
        float rotationAngle = object.getAxisData().getRotationAngle();
        float axialTilt = object.getAxisData().getAxialTilt();

        gl.glTranslatef(center.x, center.y, center.z);
        gl.glRotatef(axialTilt, 1f, 1f, 1f);
        gl.glRotatef(rotationAngle, 0f, 0f, 1f);

        com.jogamp.opengl.util.texture.Texture texture =
                (com.jogamp.opengl.util.texture.Texture)
                        ((TexturedObject)object.getShape()).getTexture().getContent();
        texture.enable(gl);
        texture.bind(gl);

        GLUquadric sphere = glu.gluNewQuadric();
        glu.gluQuadricTexture(sphere, true);
        glu.gluQuadricDrawStyle(sphere, GLU.GLU_FILL);
        glu.gluQuadricNormals(sphere, GLU.GLU_FLAT);

        if (object instanceof Light) {
            glu.gluQuadricOrientation(sphere, GLU.GLU_INSIDE);
        } else {
            glu.gluQuadricOrientation(sphere, GLU.GLU_OUTSIDE);
        }
        
        float radius = ((Sphere)shape).getRadius();
        final int slices = 32;
        final int stacks = 32;
        glu.gluSphere(sphere, radius, slices, stacks);

        //drawCircle(gl, 0, 0, radius);

        gl.glRotatef(-rotationAngle, 0f, 0f, 1f);
        gl.glRotatef(-axialTilt, 1f, 1f, 1f);
        gl.glTranslatef(-center.x, -center.y, -center.z);
    }


    @Override
    public void init(Graphics graphics) {
        JoglGraphics joglGraphics = (JoglGraphics) graphics;
        GL2 gl = (GL2) joglGraphics.get();

        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDepthFunc(GL.GL_LEQUAL);
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
        gl.glClearColor(0f, 0f, 0f, 1f);

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        // Set the light properties.
        initLightProperties(gl);
        
        // Set the material properties.
        initMaterialProperties(gl);
    }

    private void drawBackground(GL2 gl, int width, int height) {
        gl.glLoadIdentity();

        com.jogamp.opengl.util.texture.Texture texture =
                (com.jogamp.opengl.util.texture.Texture) scene.getBackgroundTexture().getContent();

        //texture.enable(gl);
        //texture.bind(gl);
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture.getTextureObject());
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0f, 0f); gl.glVertex2f(0f, 0f);
        gl.glTexCoord2f(0f, 1); gl.glVertex2f(0f, height);
        gl.glTexCoord2f(1, 1); gl.glVertex2f(width, height);
        gl.glTexCoord2f(0f, 1); gl.glVertex2f(0f, width);
        gl.glEnd();
    }

    @Override
    public void render(Graphics graphics) {
        JoglGraphics joglGraphics = (JoglGraphics) graphics;
        GL2 gl = (GL2) joglGraphics.get();
        GLU glu = new GLU();
        
        // clear screen
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        if (scene.getBackgroundTexture() != null) {
            drawBackground(gl, graphics.getWidth(), graphics.getHeight());
        }

        // re-init camera
        initCameraProperties(gl, glu, graphics.getWidth(), graphics.getHeight());

        SpaceSystem spaceSystem = scene.getSystem();

        for (int i = 0; i < spaceSystem.getSize(); i++) {
            drawPlanet(gl, glu, spaceSystem.getObject(i));
        }
    }
}
