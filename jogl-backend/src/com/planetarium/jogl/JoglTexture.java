package com.planetarium.jogl;

import com.jogamp.opengl.util.texture.TextureIO;
import com.planetarium.core.graphics.textures.Texture;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * The class of texture for JOGL library.
 */
public class JoglTexture implements Texture, Serializable {
    private static final long serialVersionUID = 1L;

    /** The JOGL texture. **/
    private com.jogamp.opengl.util.texture.Texture texture;
    /** The path to this texture. **/
    private String path;

    /** Constructs a new JOGL texture.
     * @see com.jogamp.opengl.util.texture.TextureIO
     * @param path The path to texture.
     * @param mipmap Whether mipmaps should be produced for this texture
     *               either by autogenerating them or reading them from the file
     * @param fileSuffix The suffix of the file ('png' or 'jpg' for example).
     * @throws IOException If an error occured while reading the stream. */
    public JoglTexture(String path, boolean mipmap, String fileSuffix)
            throws IOException
    {
        InputStream stream = new FileInputStream(path);
        texture = TextureIO.newTexture(stream, mipmap, fileSuffix);
        this.path = path;
    }

    /** Constructs a new JOGL texture.
     * @param path The path to texture.
     * @throws IOException If an error occured while reading the stream. */
    public JoglTexture(String path) throws IOException {
        InputStream stream = new FileInputStream(path);
        texture = TextureIO.newTexture(stream, false, null);
        this.path = path;
    }

    @Override
    public String getPath() { return path; }

    @Override
    public int getWidth() { return texture.getWidth(); }

    @Override
    public int getHeight() { return texture.getHeight(); }

    @Override
    public Object getContent() { return texture; }

}
