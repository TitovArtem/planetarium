package com.planetarium.jogl;

import com.jogamp.opengl.GL;
import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.GraphicsType;

import java.io.Serializable;

/**
 * Implements the JOGL graphics for GL2 object.
 */
public class JoglGraphics implements Graphics<GL>, Serializable {
    private static final long serialVersionUID = 1L;
    /** The values of width and height for this graphics. **/
    int width, height;
    /** The content of this graphics. **/
    private GL gl;

    /** Creates a new graphics of JOGL.
     * @param gl The GL object. */
    public JoglGraphics(GL gl, int width, int height) {
        if (gl == null) {
            throw new NullPointerException("The given Gl object is null.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid dimension.");
        }
        this.width = width;
        this.height = height;
        this.gl = gl;
    }

    @Override
    public void set(GL obj) {
        if (obj == null) {
            throw new NullPointerException("The given GL object has null value.");
        }

        gl = obj;
    }

    @Override
    public GL get() {
        return gl;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public GraphicsType getType() {
        return GraphicsType.JOGL;
    }
}
