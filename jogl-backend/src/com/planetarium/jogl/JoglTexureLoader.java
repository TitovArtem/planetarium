package com.planetarium.jogl;

import com.planetarium.core.graphics.textures.TextureLoader;
import com.planetarium.core.graphics.textures.TexturedObject;

import java.io.IOException;

/**
 * Implements the loader for JOGL textures.
 * @see com.planetarium.core.graphics.textures.TextureLoader
 */
public class JoglTexureLoader implements TextureLoader {

    @Override
    public TexturedObject load(TexturedObject object) throws IOException {
        object.setTexture(new JoglTexture(object.getPath()));
        return object;
    }
}
