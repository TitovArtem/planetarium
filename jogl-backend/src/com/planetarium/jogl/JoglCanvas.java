package com.planetarium.jogl;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.planetarium.core.graphics.Canvas;
import com.planetarium.core.graphics.Graphics;
import com.planetarium.core.graphics.GraphicsType;
import com.planetarium.core.graphics.Renderer;
import com.planetarium.core.graphics.scene.Scene;

import java.io.IOException;

/**
 * Implement the canvas for JOGL.
 */
public class JoglCanvas extends GLCanvas implements Canvas, GLEventListener {

    /** The scene. **/
    private Scene scene;
    /** The renderer for drawing scene. **/
    private Renderer renderer;

    private String bgTexturePath;

    /** Creates a new canvas for JOGL.
     * By default the renderer is {@link com.planetarium.jogl.JoglRenderer}
     * @param scene The scene which will be rendered.
     * @param width The width of this canvas.
     * @param height The height of this canvas.
     * @param capabilities The capabilities for GL. */
    public JoglCanvas(Scene scene, int width, int height, GLCapabilities capabilities) {
        super(capabilities);
        if (scene == null) {
            throw new NullPointerException("The given scene has null value.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid values for dimension.");
        }
        this.scene = scene;
        renderer = new JoglRenderer(scene);
        setSize(width, height);
        addGLEventListener(this);
    }

    /** Creates a new canvas for JOGL.
     * @param renderer The renderer for this canvas.
     * @param width The width of this canvas.
     * @param height The height of this canvas. */
    public JoglCanvas(Renderer renderer, int width, int height) {
        super(createGLCapabilities());
        if (renderer == null) {
            throw new NullPointerException("The given renderer has null value.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid values for dimension.");
        }

        scene = renderer.getScene();
        this.renderer = renderer;
        setSize(width, height);
        addGLEventListener(this);
    }

    /** Creates a new canvas for JOGL.
     * @param renderer The renderer for this canvas.
     * @param width The width of this canvas.
     * @param height The height of this canvas.
     * @param texturePath The path to background texture. */
    public JoglCanvas(Renderer renderer, int width, int height, String texturePath)
            throws IOException
    {
        super(createGLCapabilities());
        if (renderer == null) {
            throw new NullPointerException("The given renderer has null value.");
        } else if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid values for dimension.");
        } else if (texturePath == null) {
            throw new NullPointerException("The given path to texture is null");
        }

        scene = renderer.getScene();
        this.renderer = renderer;
        bgTexturePath = texturePath;
        setSize(width, height);
        addGLEventListener(this);
    }

    /** @return The capabilities for GL. **/
    public static GLCapabilities createGLCapabilities() {
        GLCapabilities capabilities = new GLCapabilities(GLProfile.getGL2ES1());
        capabilities.setRedBits(8);
        capabilities.setBlueBits(8);
        capabilities.setGreenBits(8);
        capabilities.setAlphaBits(8);
        return capabilities;
    }

    @Override
    public Renderer getRenderer() {
        return renderer;
    }

    @Override
    public void setRenderer(Renderer renderer) {
        if (renderer == null) {
            throw new NullPointerException();
        }
        this.renderer = renderer;
    }

    @Override
    public Graphics getGraphicsObject() {
        return new JoglGraphics(getGL(), getWidth(), getHeight());
    }

    @Override
    public void setGraphicsObject(final Graphics graphics) {
        if (graphics.getType() != GraphicsType.JOGL) {
            throw new IllegalArgumentException("The given graphics isn't for JOGL.");
        }

        this.setGL((GL)graphics.get());
    }

    @Override
    public void initScene(Scene scene) {
        if (scene == null) {
            throw new NullPointerException("The given scene has null value.");
        }

        this.scene = scene;
    }

    @Override
    public void drawScene() {
        this.display();
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL gl = glAutoDrawable.getGL();
        glAutoDrawable.setGL(new DebugGL2(gl.getGL2()));

        try {
            scene.loadTextures(new JoglTexureLoader());
            scene.setBackgroundTexture(new JoglTexture(bgTexturePath));
        } catch (IOException exc) {
            throw new IllegalStateException("Error to load texture.");
        }

        renderer.init(new JoglGraphics(gl, getWidth(), getHeight()));
}

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL gl = glAutoDrawable.getGL();
        renderer.render(new JoglGraphics(gl, getWidth(), getHeight()));
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL gl = glAutoDrawable.getGL();
        gl.glViewport(0, 0, width, height);
    }
}
