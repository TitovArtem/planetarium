package com.planetarium.core.graphics;

import java.io.Serializable;

/**
 * Class of RGB color. It contain the r, g, b and alpha components as integers
 * in the range [0, 255]. Every color has an implicit alpha value of 255.
 * The alpha value defines the transparency of a color.
 * If an alpha value of 255 means that the color is completely opaque, if it
 * has 0 value - the color is completely transparent.
 */
public class Color implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color WHITE = new Color(255, 255, 255);
    public static final Color RED = new Color(255, 0, 0);
    public static final Color GREEN = new Color(0, 255, 0);
    public static final Color BLUE = new Color(0, 0, 255);
    public static final Color AQUA = new Color(255, 255, 100);
    public static final Color BRIGHT_WIOLET = new Color(205, 0, 205);
    public static final Color BROWN = new Color(150, 75, 0);
    public static final Color CARROT = new Color(237, 145, 33);
    public static final Color DARK_BLUE = new Color(0, 71, 171);
    public static final Color DARK_PINK = new Color(231, 84, 128);
    public static final Color DARK_GREEN = new Color(23, 114, 69);
    public static final Color DARK_VIOLET = new Color(66, 49, 137);
    public static final Color DEEP_SKY = new Color(0, 191, 255);
    public static final Color ORANGE = new Color(255, 79, 0);
    public static final Color YELLOW = new Color(255, 255, 0);
    public static final Color GRAY = new Color(128, 128, 128);
    public static final Color DARK_GRAY = new Color(47, 79, 79);
    public static final Color CREAM = new Color(255, 253, 208);
    public static final Color BEIGE = new Color(245, 245, 220);
    public static final Color ALICE_BLUE = new Color(240, 248, 255);
    public static final Color LIME = new Color(204, 255, 0);
    public static final Color MAROON = new Color(128, 0, 0);
    public static final Color NAVY = new Color(0, 0, 128);
    public static final Color PURPLE = new Color(128, 0, 128);

    /** The red, green, blue and alpha components. **/
    private int r, g, b, a;

    /** Constructs a new color from the given components.
     * @param r The red component of color.
     * @param g The green component of color.
     * @param b The blue component of color.
     * @param a The alpha value of color. */
    public Color(int r, int g, int b, int a) {
        set(r, g, b, a);
    }

    /** Constructs a new color from the given components.
     * The value of alpha is 255.
     * @param r The red component of color.
     * @param g The green component of color.
     * @param b The blue component of color. */
    public Color(int r, int g, int b) {
        set(r, g, b);
    }

    /** Constructs a new color from the given vector.
     * @param other The color. */
    public Color(final Color other) {
        set(other);
    }

    /** @return The red component of this color. **/
    public int getRed() { return r; }

    /** @return The green component of this color. **/
    public int getGreen() { return g; }

    /** @return The blue component of this color. **/
    public int getBlue() { return b;  }

    /** @return The alpha component of this vector. **/
    public int getAlpha() { return a;  }

    /** Sets the red component of this color.
     * @param r A value for the red component. */
    public void setRed(int r) { this.r = r; }

    /** Sets the green component of this color.
     * @param g A value for the green component. */
    public void setGreen(int g) { this.g = g; }

    /** Sets the blue component of this vector.
     * @param b A value for the blue component.  */
    public void setBlue(int b) { this.b = b; }

    /** Sets the alpha component of this vector.
     * @param a A value for the alpha component. */
    public void setAlpha(int a) { this.a = a; }

    /** @return This color for changing. **/
    public Color get() { return this; }

    /** Sets all components of this color from given values.
     *  The all given arguments must have values in the range [0, 255].
     * @param r The red component of color.
     * @param g The green component of color.
     * @param b The blue component of color.
     * @param a The alpha value of color.
     * @return This color for changing. */
    public Color set(int r, int g, int b, int a) {
        checkBounds(r, g, b, a);
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        return this;
    }

    /** Sets RGB components of this color from given values.
     *  The all given arguments must have values in the range [0, 255].
     *  The value of alpha is 255.
     * @param r The red component of color.
     * @param g The green component of color.
     * @param b The blue component of color.
     * @return This color for changing. */
    public Color set(int r, int g, int b) {
        return this.set(r, g, b, 255);
    }

    /** Sets all components of this color from the given color.
     * @param other The color.
     * @return This color for changing. */
    public Color set(final Color other) {
        return this.set(other.getRed(), other.getGreen(),
                other.getBlue(), other.getAlpha());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Color color = (Color) o;

        if (r != color.r) return false;
        if (g != color.g) return false;
        if (b != color.b) return false;
        return a == color.a;
    }

    @Override
    public int hashCode() {
        int result = r;
        result = 31 * result + g;
        result = 31 * result + b;
        result = 31 * result + a;
        return result;
    }

    @Override
    public String toString() {
        return "Color{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                ", a=" + a +
                '}';
    }

    /* Checks bounds of arguments. It must be in the range [0, 255]. */
    protected void checkBounds(int r, int g, int b, int a) {
        if (r < 0 || r > 255 || g < 0 || g > 255
                || b < 0 || b > 255 || a < 0 || a > 255) {
            throw new IllegalArgumentException("Invalid values of components. " +
                    "It must be in the range [0, 255].");
        }
    }
}
