package com.planetarium.core.graphics;

import java.io.Serializable;

/**
 * Encapsulates the diffuse model of light. It has two component of the light:
 * the ambient component of the light and the power component of the light.
 * Both components can apply values in the range from 0 to 1. The maximal
 * value of the result intensity will be 1.0 - when the both components of
 * the light will be 1.0.
 */
public class DiffuseLightModel implements LightModel, Serializable {
    private static final long serialVersionUID = 1L;

    /** The ambient component of the light. **/
    private float ambient;
    /** The power component of the light source. **/
    private float amp;

    /** Factors to makes a balance between components of the light. **/
    private float ampFactor, ambientFactor;

    /** Creates a new diffuse model of light.
     * @param ambient The ambient component of the light.
     * @param amp The power component of the light. */
    public DiffuseLightModel(float ambient, float amp) {
        setAmbient(ambient);
        setLightPower(amp);
        calculateFactors();
    }

    /** @return The ambient component of the light. **/
    public float getAmbient() {
        return ambient;
    }

    /** Sets the ambient component of the light.
     * @param ambient The value for the ambient component in the range [0.0, 1.0]. */
    public void setAmbient(float ambient) {
        if (ambient < 0 || ambient > 1) {
            throw new IllegalArgumentException("The ambient " +
                    "of the light must be in range [0.0, 1.0].");
        }
        this.ambient = ambient;
        calculateFactors();
    }

    /** @return The power component of the light. **/
    public float getLightPower() {
        return amp;
    }

    /** Sets the power component of the light.
     * @param amp The value for the power component in the range [0.0, 1.0]. */
    public void setLightPower(float amp) {
        if (amp < 0 || amp > 1) {
            throw new IllegalArgumentException("The power " +
                    "of the light must be in range [0.0, 1.0].");
        }
        this.amp = amp;
        calculateFactors();
    }

    @Override
    public float getIntensity(float primaryIntensity) {
        if (primaryIntensity < 0) {
            primaryIntensity = 0f;
        } else if (primaryIntensity > 1) {
            primaryIntensity = 1f;
        }

        return ambientFactor + ampFactor * primaryIntensity;
    }

    private void calculateFactors() {
        ambientFactor = ambient / (ambient + amp);
        ampFactor = amp / (ambient + amp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiffuseLightModel that = (DiffuseLightModel) o;

        if (Float.compare(that.ambient, ambient) != 0) return false;
        if (Float.compare(that.amp, amp) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (ambient != +0.0f ? Float.floatToIntBits(ambient) : 0);
        result = 31 * result + (amp != +0.0f ? Float.floatToIntBits(amp) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DiffuseLightModel{" +
                "ambient=" + ambient +
                ", amp=" + amp +
                '}';
    }
}
