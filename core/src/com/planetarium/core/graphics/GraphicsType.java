package com.planetarium.core.graphics;

/**
 * Contains all types of graphics, see {@link com.planetarium.core.graphics.Graphics}.
 * For new type or unknown graphics there is type OTHER.
 */
public enum GraphicsType {
    JOGL, SWING, OTHER
}
