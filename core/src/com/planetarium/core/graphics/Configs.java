package com.planetarium.core.graphics;

import java.io.Serializable;

/**
 * Encapsulates the configs for renderer.
 */
public class Configs implements Serializable {
    public static final int NO_SELECTED_INDEX = -1;
    private static final long serialVersionUID = 1L;
    private int selectedObjectIndex = NO_SELECTED_INDEX;
    private Color selectedCaptionColor = Color.BLUE;
    private Color captionColor = Color.WHITE;
    private boolean isCaptions = true;

    public Color getCaptionColor() {
        return captionColor;
    }

    public void setCaptionColor(Color captionColor) {
        this.captionColor = captionColor;
    }

    public int getSelectedObjectIndex() {
        return selectedObjectIndex;
    }

    public void setSelectedObjectIndex(int selectedObjectIndex) {
        this.selectedObjectIndex = selectedObjectIndex;
    }

    public Color getSelectedCaptionColor() {
        return selectedCaptionColor;
    }

    public void setSelectedCaptionColor(Color selectedCaptionColor) {
        this.selectedCaptionColor = selectedCaptionColor;
    }

    public boolean isCaptions() {
        return isCaptions;
    }

    public void setIsCaptions(boolean isCaptions) {
        this.isCaptions = isCaptions;
    }
}
