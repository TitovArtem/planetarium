package com.planetarium.core.graphics;

import com.planetarium.core.graphics.scene.Scene;

/**
 * Describes the common interface for the canvas which
 * will be realized by graphic library.
 */
public interface Canvas {

    /** @return The renderer of this canvas for changing. **/
    Renderer getRenderer();

    /** Sets the renderer for this canvas. **/
    void setRenderer(Renderer renderer);

    /** @return The object of graphics. **/
    Graphics getGraphicsObject();

    /** Sets the object of graphics for this canvas.
     * @param graphics The object of graphics. */
    void setGraphicsObject(Graphics graphics);

    /** Initializes the given scene for this canvas.
     * @param scene The scene for this canvas. */
    void initScene(Scene scene);

    /** Draws the scene. **/
    void drawScene();
}
