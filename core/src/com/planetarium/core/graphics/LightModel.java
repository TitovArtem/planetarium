package com.planetarium.core.graphics;

/**
 * Describes the light model for the scene.
 */
public interface LightModel {

    /** Calculates the light intensity depending on the parameters of
     * the environment and the light source.
     * @param primaryIntensity The primary light intensity.
     * @return A new light intensity. Typically, it's in the range from 0 to 1. */
    float getIntensity(float primaryIntensity);
}