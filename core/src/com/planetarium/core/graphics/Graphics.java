package com.planetarium.core.graphics;

/**
 * Encapsulates the essential objects of graphic library.
 */
public interface Graphics<T> {

    /** Sets the content of this graphics.
     * @param obj The content for this graphics. */
    void set(T obj);

    /** @return The content of this graphics. **/
    T get();

    /** @return The width of the display surface for this graphics. **/
    int getWidth();

    /** @return The height of the display surface for this graphics. **/
    int getHeight();

    /** @return The type of this graphics. **/
    GraphicsType getType();

}
