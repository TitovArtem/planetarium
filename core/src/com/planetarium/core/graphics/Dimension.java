package com.planetarium.core.graphics;

import java.io.Serializable;

/**
 * Class of dimension of square object.
 * It has width and height.
 */
public class Dimension implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The height component. **/
    private int height;
    /** The width component. **/
    private int width;

    /** Constructs a new dimension from the given values.
     * @param height The height.
     * @param width The width. */
    public Dimension(int width, int height) {
        if (width * height <= 0) {
            throw new IllegalArgumentException("A components of a dimension" +
                    "must be positive.");
        }
        this.height = height;
        this.width = width;
    }

    /** Constructs a new dimension from the given dimension.
     * @param other A dimension. */
    public Dimension(Dimension other) {
        this.height = other.getHeight();
        this.width = other.getWidth();
    }

    /** @return The height component. **/
    public int getHeight() { return height; }

    /** @return The width component. **/
    public int getWidth() { return width; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dimension dimension = (Dimension) o;

        if (height != dimension.height) return false;
        return width == dimension.width;

    }

    @Override
    public int hashCode() {
        int result = height;
        result = 31 * result + width;
        return result;
    }

    @Override
    public String toString() {
        return "Dimension{" +
                "height=" + height +
                ", width=" + width +
                '}';
    }
}
