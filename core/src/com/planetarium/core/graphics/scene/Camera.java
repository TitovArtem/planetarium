package com.planetarium.core.graphics.scene;

import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Matrix4;
import com.planetarium.core.math.Vector3;

import java.io.Serializable;

/**
 * Describes a cameras in the scene.
 */
public class Camera implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The position of the camera. **/
    private Vector3 position = new Vector3();
    /** The direction of the camera. **/
    private Vector3 direction = new Vector3(0, 0, -1);
    /** The up-vector of this camera. **/
    private Vector3 up = new Vector3(0, 1, 0);

    /** The view matrix. **/
    private Matrix4 view = new Matrix4();
    /** The projection matrix. **/
    private Matrix4 projection = new Matrix4();
    /** The viewport matrix. **/
    private Matrix4 viewport = new Matrix4();
    /** The frustum matrix. **/
    private Matrix4 frustum = new Matrix4();
    
    /** Constructs a new camera from the given position of camera.
     * @param position A position of the camera. */
    public Camera(final Vector3 position) {
        this.position.set(position);
    }

    /** Constructs a new camera from the given position
     * and direction of camera.
     * @param position A position of the camera.
     * @param direction A direction of the camera. */
    public Camera(final Vector3 position, final Vector3 direction) {
        this.position.set(position);
        this.direction.set(direction);
    }

    /** Constructs a new camera from the given position,
     * direction and up-vector of camera.
     * @param position A position of the camera.
     * @param direction A direction of the camera.
     * @param up A up-vector of the camera. */
    public Camera(final Vector3 position, final Vector3 direction, final Vector3 up) {
        this.position.set(position);
        this.direction.set(direction);
        this.up.set(up);
    }

    /** @return The vector of position of this camera for changing. **/
    public Vector3 getPosition() { return position; }

    /** Sets a new position of this camera.
     * @param position A new position of this camera. */
    public void setPosition(final Vector3 position) {
        this.position.set(position);
    }

    /** @return The vector of direction of this camera for changing. **/
    public Vector3 getDirection() { return direction; }

    /** Sets a new direction of this camera.
     * @param direction A new direction of this camera. */
    public void setDirection(final Vector3 direction) {
        this.direction.set(direction);
    }

    /** @return The up-vector of this camera for changing. **/
    public Vector3 getUp() { return up; }

    /** Sets a new up-vector of this camera.
     * @param up A new up-vector of this camera. */
    public void setUp(final Vector3 up) {
        this.up.set(up);
    }

    /** Calculates the viewport matrix.
     * @param x The lower-left corner of the viewport rectangle, in pixels.
     * @param y The lower-left corner of the viewport rectangle, in pixels.
     * @param w The width of the viewport.
     * @param h The height of the viewport.
     * @return The viewport matrix of this camera for changing. */
    public Matrix4 calculateViewport(int x, int y, int w, int h) {
        viewport = Matrix4.getIdentity();
        final float depth = 2000f;
        viewport.set(0, 3, x + w / 2.f);
        viewport.set(1, 3, y + h / 2.f);
        viewport.set(2, 3, depth / 2.f);
        viewport.set(0, 0, w / 2.f);
        viewport.set(1, 1, h / 2.f);
        viewport.set(2, 2, depth / 2.f);

        return viewport;
    }

    /** @return The viewport matrix of this camera for changing. **/
    public Matrix4 getViewport() {
        return viewport;
    }

    /** Calculates the frustum matrix.
     * @param left The coordinate for the left-vertical clipping plane.
     * @param right The coordinate for the right-vertical clipping plane.
     * @param bottom The coordinate for the bottom-horizontal clipping plane.
     * @param top The coordinate for the top-horizontal clipping plane.
     * @param zNear The distances to the near-depth clipping plane. Must be positive.
     * @param zFar The distances to the far-depth clipping plane. Must be positive.
     * @return The frustum matrix of this camera for changing. */
    public Matrix4 calculateFrustum(float left, float right, float bottom,
                                     float top, float zNear, float zFar)
    {
        frustum = new Matrix4();
        float temp = 2.0f * zNear;
        float temp2 = right - left;
        float temp3 = top - bottom;
        float temp4 = zFar - zNear;

        frustum.set(0, 0, temp / temp2);
        frustum.set(1, 1, temp / temp3);
        frustum.set(0, 2, (right + left) / temp2);
        frustum.set(1, 2, (top + bottom) / temp3);
        frustum.set(2, 2, (-zFar - zNear) / temp4);
        frustum.set(3, 2, -1.0f);
        frustum.set(2, 3, (-temp * zFar) / temp4);
        
        return frustum;
    }

    /** @return The frustum matrix of this camera for changing. **/
    public Matrix4 getFrustum() {
        return frustum;
    }

    /** Calculates the perspective projection matrix.
     * @param fovy The field of view angle, in degrees, in the y direction.
     * @param aspect The aspect ratio that determines the field of view
     *               in the x direction. The aspect ratio is the ratio of x (width) to y (height).
     * @param zNear The distance from the viewer to the near
     *              clipping plane (must be positive).
     * @param zFar The distance from the viewer to the far
     *             clipping plane (must be positive)
     * @return The projection matrix of this camera for changing. */
    public Matrix4 calculatePerspective(float fovy, float aspect, float zNear, float zFar) {
        if (zNear < 0 || zFar < 0) {
            throw new IllegalArgumentException("The zNear and zFar " +
                    "arguments must be positive.");
        }
        float yMax = zNear * FloatMath.tan(fovy * FloatMath.PI / 360);
        float xMax = yMax * aspect;
        projection = calculateFrustum(-xMax, xMax, -yMax, yMax, zNear, zFar);
        return projection;
    }

    /** @return The projection matrix of this camera for changing. **/
    public Matrix4 getProjection() {
        return projection;
    }

    /** Calculates the view matrix of this camera.
     * @return The view matrix of this camera for changing. */
    public Matrix4 calculateView() {

        Vector3 z = ((position.copy()).sub(direction)).normalize();
        Vector3 x = ((up.copy()).cross(z)).normalize();
        Vector3 y = ((z.copy()).cross(x)).normalize();

        view = Matrix4.getIdentity();

        view.set(0, 0, x.x);
        view.set(1, 0, y.x);
        view.set(2, 0, z.x);

        view.set(0, 1, x.y);
        view.set(1, 1, y.y);
        view.set(2, 1, z.y);

        view.set(0, 2, x.z);
        view.set(1, 2, y.z);
        view.set(2, 2, z.z);

        Matrix4 model = Matrix4.getIdentity();
        model.set(0, 3, position.x);
        model.set(1, 3, position.y);
        model.set(2, 3, position.z);

        return view.multiple(model);
    }

    /** @return The view matrix of this camera for changing. **/
    public Matrix4 getView() { return view; }

    /** Calculates the new direction of the camera from the given point.
     * @param x The x-component of the vector to look at.
     * @param y The y-component of the vector to look at.
     * @param z The z-component of the vector to look at. */
    public void lookAt(float x, float y, float z) {
        Vector3 temp = new Vector3(x, y, z);
        temp.sub(position).normalize();
        if (!temp.isZero()) {
            float dot = temp.dot(up);
            if (FloatMath.isEqual(dot, 1)) {
                up.set(direction).multiple(-1);
            } else if (FloatMath.isEqual(dot, -1)) {
                up.set(direction);
            }
            direction.set(temp);
            temp.set(direction).cross(up).normalize();
            up.set(temp).cross(direction).normalize();
        }
    }

    /** Calculates the new direction of the camera from the given point.
     * @param target A vector for a new direction of this camera. */
    public void lookAt(final Vector3 target) {
        lookAt(target.x, target.y, target.z);
    }
}
