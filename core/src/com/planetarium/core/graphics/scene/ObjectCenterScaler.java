package com.planetarium.core.graphics.scene;

import com.planetarium.core.math.ScaleMachine;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.geom.Shape3D;

import java.io.Serializable;

/**
 * Allows to scale the center of space objects.
 * By default the all factors have values like 1.0.
 */
public class ObjectCenterScaler implements ScaleMachine, Serializable {
    static private final long serialVersionUID = 1L;

    /** The factor for scaling of x-component of the space object's center. **/
    protected float xFactor;
    /** The factor for scaling of y-component of the space object's center. **/
    protected float yFactor;
    /** The factor for scaling of z-component of the space object's center. **/
    protected float zFactor;

    /** Constructs a new scale machine for center of objects.
     * @param xFactor The factor for scaling of x-component of center.
     * @param yFactor The factor for scaling of y-component of center.
     * @param zFactor The factor for scaling of z-component of center. */
    public ObjectCenterScaler(float xFactor, float yFactor, float zFactor) {
        this.xFactor = xFactor;
        this.yFactor = yFactor;
        this.zFactor = zFactor;
    }

    /** Constructs a new scale machine for center of objects. **/
    public ObjectCenterScaler() {
        xFactor = yFactor = zFactor = 1.0f;
    }

    /** @return The scaling factors. **/
    public Vector3 getFactors() {
        return new Vector3(xFactor, yFactor, zFactor);
    }

    /** Sets scaling factors for space objects center.
     * @param factors The scaling factors for space objects center. */
    public void setFactors(final Vector3 factors) {
        xFactor = factors.x;
        yFactor = factors.y;
        zFactor = factors.z;
    }

    @Override
    public Shape3D scale(Shape3D shape) {
        Vector3 center = shape.getCenter();
        center.x *= xFactor;
        center.y *= yFactor;
        center.z *= zFactor;
        return shape;
    }
}
