package com.planetarium.core.graphics.scene;

import com.planetarium.core.graphics.DiffuseLightModel;
import com.planetarium.core.graphics.LightModel;
import com.planetarium.core.graphics.textures.Texture;
import com.planetarium.core.graphics.textures.TextureLoader;
import com.planetarium.core.graphics.textures.TexturedObject;
import com.planetarium.core.math.ScaleMachine;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.objects.Light;
import com.planetarium.core.objects.SpaceSystem;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates the elements of scenes. Each scene has a space system,
 * array of cameras, array of lights (count of lights must be less then 8) and
 * the light model. By default, the light model is the diffuse model
 * where the power component is 0.9 and the ambient component is 0.4.
 * There is current camera - it's one element from array of cameras.
 * If array of cameras is empty you can use default camera.
 * @see com.planetarium.core.graphics.DiffuseLightModel
 */
public class Scene implements Serializable {
    /** The max count of lights. **/
    public static final int MAX_LIGHTS = 7;
    private static final long serialVersionUID = 1L;
    /** The default camera. It uses when array of cameras is empty. **/
    private final Camera defaultCamera = new Camera(new Vector3(0, 0, 0));
    /** The space system of this scene. **/
    private SpaceSystem system;
    /** The array of cameras of this scene. **/
    private ArrayList<Camera> cameras;
    /** The array of lights. (It must have length no more then 7) **/
    private ArrayList<Light> lights;
    /** The scale machine for objects of space system. **/
    private ScaleMachine scaleMachine = new ObjectCenterScaler();
    /** The light model of this scene. **/
    private LightModel lightModel = new DiffuseLightModel(0.15f, 0.95f);
    /** The current camera. **/
    private Camera currentCamera;
    /** The background texture of this scene. **/
    private Texture bgTexture;

    /** Constructs a new scene from the given space system.
     * It sets the space system of this scene like reference at
     * the given space system.
     * @param system The space system for this scene. */
    public Scene(SpaceSystem system) {
        if (system == null) {
            throw new NullPointerException("The given space system has null value.");
        }

        this.system = system;
        cameras = new ArrayList<>();
        lights = new ArrayList<>(MAX_LIGHTS);
    }

    /** @return The background texture of this scene. **/
    public Texture getBackgroundTexture() {
        return bgTexture;
    }

    /** Sets the background texture for this scene.
     * @param bgTexture The background texture for this scene. */
    public void setBackgroundTexture(Texture bgTexture) {
        this.bgTexture = bgTexture;
    }

    /** @return The light model of this scene for changing. **/
    public LightModel getLightModel() {
        return lightModel;
    }

    /** Sets the light model to this scene.
     * @param lightModel The light model for this scene. */
    public void setLightModel(LightModel lightModel) {
        if (lightModel == null) {
            throw new NullPointerException("The given light model has null value.");
        }

        this.lightModel = lightModel;
    }

    /** @return The space system of this scene for changing. **/
    public SpaceSystem getSystem() { return system; }

    /** Sets the space system of this scene like reference at
     * the given space system.
     * @param system A space system for reference. */
    public void setSystem(SpaceSystem system) {
        this.system = system;
    }

    /** Adds the given camera to cameras array.
     * @param camera A new camera. */
    public void addCamera(Camera camera) {
        cameras.add(camera);
    }

    /** Adds the given camera to cameras array at the given position.
     * @param i The index for adding a new camera.
     * @param camera A new camera. */
    public void addCamera(int i, Camera camera) {
        cameras.add(i, camera);
    }

    /** Removes camera from cameras array at the given position.
     * @param i The index of element which will be removed. */
    public void removeCamera(int i) {
        cameras.remove(i);
    }

    /** @return The array of cameras for changing.  */
    public List<Camera> getCameras() {
        return cameras;
    }

    /** Sets the array of cameras.
     * @param cameras A new array of cameras. */
    public void setCameras(final List<Camera> cameras) {
        this.cameras = (ArrayList<Camera>)cameras.subList(0, cameras.size());
    }

    /** @return The current camera for changing. **/
    public Camera getCurrentCamera() {
        return currentCamera;
    }

    /** Sets current camera from the array of cameras at the given index.
     * @param i The index of the current camera in the array of cameras. */
    public void setCurrentCamera(int i) {
        currentCamera = cameras.get(i);
    }

    /** Adds a new light to array of lights.
     * @param light A new light. */
    public void addLight(Light light) {
        if (lights.size() == MAX_LIGHTS) {
            throw new ArrayIndexOutOfBoundsException(
                    "The count of lights must be less then 8.");
        }
        lights.add(light);
    }

    /** Adds a new light to array of lights at the given position.
     * @param i The index for adding a new light to array of lights.
     * @param light A new light. */
    public void addLight(int i, Light light) {
        if (lights.size() == MAX_LIGHTS) {
            throw new ArrayIndexOutOfBoundsException(
                    "The count of lights must be less then 8.");
        }
        lights.add(i, light);
    }

    /** Removes the light from array of lights at the given position.
     * @param i The index of element which will be removed. */
    public void removeLight(int i) {
        lights.remove(i);
    }

    /** Sets light to array of lights at the given position.
     * @param i The index for new light in the array of lights.
     * @param light A new light. */
    public void setLight(int i, Light light) {
        lights.set(i, light);
    }

    /** @return The light from array of lights at the given
     * position for changing. */
    public Light getLight(int i) {
        return lights.get(i);
    }

    /** @return The number of lights in this scene. **/
    public int getLightsCount() { return lights.size(); }

    /** Loads textures for textured objects of system.
     * @param loader Loader to load textures.
     * @throws IOException If an error occured while reading the stream. */
    public void loadTextures(final TextureLoader loader) throws IOException {
        for (int i = 0; i < system.getSize(); i++) {
            if (system.getObject(i).getShape() instanceof TexturedObject) {
                TexturedObject object = (TexturedObject) system.getObject(i).getShape();
                loader.load(object);
            }
        }
    }

    /** Loads textures for objects which wasn't textured.
     * @param loader Loader to load textures.
     * @throws IOException If an error occured while reading the stream. */
    public void reloadTextures(final TextureLoader loader) throws IOException {
        for (int i = 0; i < system.getSize(); i++) {
            if (system.getObject(i).getShape() instanceof TexturedObject) {
                TexturedObject shape = (TexturedObject) system.getObject(i).getShape();
                if (shape.isTextureLoaded()) loader.load(shape);
            }
        }
    }

    /** @return The scale machine for changing. **/
    public ScaleMachine getScaleMachine() {
        return scaleMachine;
    }

    /** Sets the scale machine of this scene.
     * @param scaleMachine A new scale machine. */
    public void setScaleMachine(ScaleMachine scaleMachine) {
        if (scaleMachine == null) {
            throw new NullPointerException("The given scale machine has null value.");
        }
        this.scaleMachine = scaleMachine;
    }
}
