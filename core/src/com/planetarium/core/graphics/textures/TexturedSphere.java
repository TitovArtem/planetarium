package com.planetarium.core.graphics.textures;

import com.planetarium.core.math.geom.Sphere;

import java.io.Serializable;

/**
 * Encapsulates a sphere which has texture.
 *
 */
public class TexturedSphere extends Sphere implements TexturedObject, Serializable {
    private static long serialVersionUID = 1L;

    /** The texture of this sphere. It can has null value if
     * the texture wasn't uploaded. **/
    private Texture texture = null;
    /** The path to texture file. **/
    private String path;

    /** Constructs a new textured sphere.
     * @param other A sphere.
     * @param path A path to texture file. */
    public TexturedSphere(Sphere other, String path) {
        super(other);
        this.path = path;
    }

    @Override
    public Texture getTexture() { return texture; }

    @Override
    public void setTexture(Texture texture) { this.texture = texture; }

    @Override
    public String getPath() { return path; }

    @Override
    public void setPath(String path) { this.path = path; }

    @Override
    public boolean isTextureLoaded() {
        return texture == null;
    }
}
