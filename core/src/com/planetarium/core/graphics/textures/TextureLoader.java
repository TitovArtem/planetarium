package com.planetarium.core.graphics.textures;

import java.io.IOException;

/**
 * Allows loading a texture for object from it's path of a texture file.
 */
public interface TextureLoader {

    /** Loads texture to file from it's path
     * @param object A textured object which has a path to texture file.
     * @return A textured object with loaded texture.
     * @throws IOException If is error of loading a texture. */
    TexturedObject load(TexturedObject object) throws IOException;
}
