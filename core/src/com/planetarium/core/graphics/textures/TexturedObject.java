package com.planetarium.core.graphics.textures;

/**
 * Describes a behaviour of objects which has a texture.
 * @see com.planetarium.core.graphics.textures.Texture
 */
public interface TexturedObject {
    /** @return The texture of this object. **/
    Texture getTexture();

    /** Sets the texture of this object.
     * @param texture A texture for this object. */
    void setTexture(Texture texture);

    /** @return The path of this object. **/
    String getPath();

    /** Sets the path of this object.
     * @param path A path for this object. */
    void setPath(String path);

    /** @return True if the texture of this object isn't null. **/
    boolean isTextureLoaded();
}
