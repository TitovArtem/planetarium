package com.planetarium.core.graphics.textures;

/**
 * Describes all types of textures.
 * All of them contains data which depends on graphic library what you use.
 */
public interface Texture {
    /** @return The path to data of this texture. **/
    String getPath();

    /** @return The width of this texture. **/
    int getWidth();

    /** @return The height of this texture. **/
    int getHeight();

    /** @return The content of this texture. **/
    Object getContent();
}
