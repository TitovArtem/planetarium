package com.planetarium.core.graphics;

import com.planetarium.core.graphics.scene.Scene;

/**
 * Describes the interface for rendering of the scene.
 */
public abstract class Renderer {

    /** The reference to scene for this renderer. **/
    protected Scene scene;

    /** @return The scene of this renderer for changing. **/
    public Scene getScene() {
        return scene;
    }

    /** Sets the scene which will be drawn by this renderer. It doesn't copy
     * the given scene, it sets the reference at the given scene.
     * @param scene The scene for rendering. */
    public void setScene(Scene scene) {
        if (scene == null) {
            throw new NullPointerException("The given scene has null value.");
        }
        this.scene = scene;
    }

    /** Initialises the graphics to draw the scene.
     * @param graphics The graphics for initialising. */
    public abstract void init(Graphics graphics);

    /** Renders the scene for the given graphics.
     * @param graphics The graphics for rendering the scene. */
    public abstract void render(Graphics graphics);
}