package com.planetarium.core.loader;


/**
 * Encapsulates common fields for {@link com.planetarium.core.objects.Star}
 * and {@link com.planetarium.core.objects.Planet}.
 */
class UniversalObject {
    // AxisData
    public double period;
    public float axialTilt;

    // Orbit
    public double semiMajorAxis;
    public double eccentricity;
    public double inclination;
    public double longitude;
    public double periapsisArgument;
    public double meanAnomaly;


    // Sphere
    public float radius;
    public String texture;

    // Star
    public float intensity;
    public int rColor, gColor, bColor;


    public String info;
}
