package com.planetarium.core.loader;

import com.planetarium.core.objects.SpaceObject;

import java.io.IOException;

/**
 * Describes the interface to load space objects.
 */
public interface SpaceObjectLoader {

    /** @return The space object which was loaded.
      * @throws IOException If there is error of deserialization. */
    SpaceObject load() throws IOException;
}
