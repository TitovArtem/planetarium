package com.planetarium.core.loader;

import com.planetarium.core.objects.SpaceSystem;

import java.io.IOException;

/**
 * Describes the interface to load space systems.
 */
public interface SystemLoader {

    /** @return The space system which was loaded.
     * @throws IOException If there is error of deserialization. */
    SpaceSystem load() throws IOException;
}
