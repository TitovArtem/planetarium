package com.planetarium.core.loader;

import com.planetarium.core.graphics.Color;
import com.planetarium.core.graphics.textures.TexturedSphere;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.objects.Planet;
import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceObjectInfo;
import com.planetarium.core.objects.Star;
import com.planetarium.core.physics.axis.AxisData;
import com.planetarium.core.physics.orbit.Orbit;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.PatternSyntaxException;

/**
 * Implements loader for {@link com.planetarium.core.loader.UniversalObject}.
 */
public class SphereObjectLoader implements SpaceObjectLoader {
    /** The path of JSON file. **/
    private String path;
    /** If it is true - the object is a star. **/
    private boolean isLight;

    /** Creates a new loader.
     * @param path The path to JSON file. */
    public SphereObjectLoader(String path, boolean isLight) {
        if (path == null) {
            throw new NullPointerException();
        }
        this.path = path;
        this.isLight = isLight;
    }

    /** @return The path to JSON file. **/
    public String getPath() {
        return path;
    }

    /** Sets the path to JSON file.
     * @param path The path to JSON file. */
    public void setPath(String path) {
        if (path == null) {
            throw new NullPointerException();
        }
        this.path = path;
    }

    /** @return The type of loading object. **/
    public boolean isLight() {
        return isLight;
    }

    /** Sets the type of loading object.
     * @param isLight If it's true - will load the star. */
    public void setIsLight(boolean isLight) {
        this.isLight = isLight;
    }

    @Override
    public SpaceObject load() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        UniversalObject t = mapper.readValue(
                new FileInputStream(path), UniversalObject.class);

        SpaceObject object;
        Orbit orbit = new Orbit(t.semiMajorAxis, t.eccentricity, t.inclination,
                t.longitude, t.periapsisArgument, t.meanAnomaly);
        AxisData axisData = new AxisData(t.axialTilt);
        if (t.period == 0) {
            axisData.setAngularSpeed(0);
        } else {
            axisData.setAngularSpeedFromPeriod(t.period);
        }

        if (isLight) {
            if (t.intensity > 1 || t.intensity < 0) {
                throw new IOException("The value of intensity " +
                        "must be in range [0, 1].");
            }
            Star star = new Star(new TexturedSphere(new Sphere(t.radius), t.texture),
                    t.intensity, new Color(t.rColor, t.gColor, t.bColor));
            star.setOrbit(orbit);
            star.setAxisData(axisData);

            object = star;
        } else {
            object = new Planet(new TexturedSphere(new Sphere(t.radius), t.texture),
                    orbit, axisData);
        }

        try {
            SpaceObjectInfo info;
            if (!t.info.equals("")) {
                SpaceObjectInfoParser parser = new SpaceObjectInfoParser();
                info = parser.parse(t.info);
            } else {
                info = new SpaceObjectInfo();
            }
            object.setInfo(info);
        } catch (IndexOutOfBoundsException exc) {
            System.err.println("Invalid number of fields.");
        } catch (PatternSyntaxException exc) {
            System.err.println("Invalid pattern for the given info.");
        }

        return object;
    }

}
