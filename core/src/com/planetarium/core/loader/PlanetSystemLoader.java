package com.planetarium.core.loader;

import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceSystem;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Implements the loader for planet systems.
 */
public class PlanetSystemLoader implements SystemLoader {

    /** The path of JSON file. **/
    private String path;

    /** Creates a new loader.
     * @param path The path to JSON file. */
    public PlanetSystemLoader(String path) {
        if (path == null) {
            throw new NullPointerException();
        }
        this.path = path;
    }

    /** @return The path to JSON file. **/
    public String getPath() {
        return path;
    }

    /** Sets the path to JSON file.
     * @param path The path to JSON file. */
    public void setPath(String path) {
        if (path == null) {
            throw new NullPointerException();
        }
        this.path = path;
    }


    @Override
    public SpaceSystem load() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        Map map = mapper.readValue(
                new FileInputStream(path), Map.class);
        ProxySystem proxySystem = new ProxySystem();

        try {
            proxySystem.stars = (List<String>) map.get("stars");
            proxySystem.planets = (List<String>) map.get("planets");
        } catch (ClassCastException exc) {
            throw new IOException("Invalid structure of file.");
        }

        if (proxySystem.planets == null || proxySystem.stars == null) {
            throw new IOException("Invalid structure of file.");
        }

        ArrayList<SpaceObject> objects = new ArrayList<>();
        SphereObjectLoader loader = new SphereObjectLoader("", true);


        for (int i = 0; i < proxySystem.stars.size(); i++) {
            loader.setPath(proxySystem.stars.get(i));
            objects.add(loader.load());
        }


        loader.setIsLight(false);
        for (int i = 0; i < proxySystem.planets.size(); i++) {
            loader.setPath(proxySystem.planets.get(i));
            objects.add(loader.load());
        }

        return new SpaceSystem(objects);
    }

    private class ProxySystem {
        List<String> stars;
        List<String> planets;

        public ProxySystem() {
            stars = new ArrayList<>();
            planets = new ArrayList<>();
        }
    }

}
