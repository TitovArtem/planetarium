package com.planetarium.core.loader;

import com.planetarium.core.objects.SpaceObjectInfo;

/**
 * Implements the information parser for two keys.
 */
public class SpaceObjectInfoParser implements InformationParser {
    /** The primary token to split string for couples. **/
    private String mainToken = "##";
    /** The deep token to split string for the couple: field -> description. */
    private String deepToken = ":=";

    /** Creates a new parser.
     * @param mainToken The primary token to split string for couples.
     * @param deepToken The deep token to split string
     *                  for the couple: field -> description. */
    public SpaceObjectInfoParser(String mainToken, String deepToken) {
        this.mainToken = mainToken;
        this.deepToken = deepToken;
    }

    /** Creates a new parser where by default primary key is '$$'
     * and deep key is ':='. */
    public SpaceObjectInfoParser() {}

    @Override
    public SpaceObjectInfo parse(String info) {
        if (info == null) {
            throw new NullPointerException();
        }

        String[] fields = info.split(mainToken);

        SpaceObjectInfo objectInfo = new SpaceObjectInfo();

        for (int i = 0; i < fields.length; i++) {
            String[] subFields = fields[i].split(deepToken);
            objectInfo.add(subFields[0], subFields[1]);
        }

        return objectInfo;
    }
}
