package com.planetarium.core.loader;

import com.planetarium.core.objects.SpaceObjectInfo;

/**
 * Describes the interface to parse the space object
 * information {@link SpaceObjectInfo} from the given string.
 */
public interface InformationParser {

    /** Parses the space object information from the given string.
     * @param info The information for parsing.
     * @return The space object information. */
    SpaceObjectInfo parse(String info);
}
