package com.planetarium.core.math.geom;

import com.planetarium.core.math.Vector3;

import java.io.Serializable;

/**
 * Class of sphere in 3D.
 */
public class Sphere extends Shape3D implements Serializable {

    private static final long serialVersionUID = 1L;

    /** The radius of the sphere. **/
    private float radius;

    /** Constructs a new sphere from the given vector of center
     * and the value of radius.
     * @param center The center vector.
     * @param radius The positive value of radius.  */
    public Sphere(Vector3 center, float radius) {
        setRadius(radius);
        this.center = new Vector3(center);
    }

    /** Constructs a new sphere from the given values of center
     * and the value of radius.
     * @param x The x-component of center vector.
     * @param y The y-component of center vector.
     * @param z The z-component of center vector.
     * @param radius The positive value of radius.  */
    public Sphere(float x, float y, float z, float radius) {
        setRadius(radius);
        center = new Vector3(x, y, z);
    }

    /** Constructs a new sphere from the given sphere.
     * @param other The sphere. */
    public Sphere(final Sphere other) {
        center = new Vector3(other.getCenter());
        radius = other.getRadius();
    }

    /** Constructs a new sphere from the given radius.
     * @param radius The radius of the sphere. */
    public Sphere(float radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must have positive value.");
        }
        center = new Vector3(0, 0, 0);
        this.radius = radius;
    }

    /** @return A copy of this sphere. **/
    public Sphere copy() {
        return new Sphere(center, radius);
    }

    /** Sets the radius of this sphere from the given value.
     * @param radius The positive value of radius. */
    public void setRadius(float radius) {
        if (this.radius < 0) {
            throw new IllegalArgumentException("Radius must have positive value.");
        }
        this.radius = radius;
    }

    /** @return The value of radius of this sphere. **/
    public float getRadius() { return radius; }

    /** @return The volume of this sphere. **/
    public float volume() {
        return (float)((4f / 3f) * Math.PI * radius * radius * radius);
    }

    /** @return The surface area of this sphere. **/
    public float surfaceArea() {
        return (float)(4 * Math.PI * radius * radius);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sphere sphere = (Sphere) o;

        if (Float.compare(sphere.radius, radius) != 0) return false;
        return !(center != null ? !center.equals(sphere.center) : sphere.center != null);

    }

    @Override
    public int hashCode() {
        int result = center != null ? center.hashCode() : 0;
        result = 31 * result + (radius != +0.0f ? Float.floatToIntBits(radius) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Sphere{" +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }
}
