package com.planetarium.core.math.geom;

import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.Vertex;

import java.io.Serializable;

/**
 * Class of triangle in 3D area.
 */
public class Triangle implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The first vertex of triangle (a-point). **/
    private Vertex a;
    /** The second vertex of triangle (b-point). **/
    private Vertex b;
    /** The third vertex of triangle (c-point). **/
    private Vertex c;
    /** The center of triangle. **/
    private Vector3 center;
    /** The normal vector of triangle.**/
    private Vector3 normal;

    /** Constructs this triangle from the given vertexes.
     * @param a The first vertex.
     * @param b The second vertex.
     * @param c The third vertex. */
    public Triangle(Vertex a, Vertex b, Vertex c) {
        if (a == null || b == null || c == null) {
            throw new NullPointerException("The given vector has null value.");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /** Constructs this triangle from the given triangle.
     * @param other The triangle. */
    public Triangle(Triangle other) {
        a = other.getA().copy();
        b = other.getB().copy();
        c = other.getC().copy();
    }

    /** @return A copy of this triangle. */
    public Triangle copy() {
        return new Triangle(this);
    }

    /** @return The vector of this a-point for changing. */
    public Vertex getA() { return a; }

    /** @return The vector of this b-point for changing. */
    public Vertex getB() { return b; }

    /** @return The vector of this c-point for changing. */
    public Vertex getC() { return c; }

    /** Gets the center of this triangle.
     * Don't forget to calculate a new center of this triangle if you
     * change values of vertexes. Use method for it: {@link Triangle#calculateCenter}.
     * @return The vector of the center of this triangle for changing. */
    public Vector3 getCenter() {
        if (center == null)
            calculateCenter();
        return center;
    }

    /** Gets the normal vector of this triangle.
     * Don't forget to calculate a new normal vector of this triangle if you
     * change values of vertexes. Use method for it: {@link Triangle#calculateNormal}.
     * @return The normal vector of this triangle for changing. */
    public Vector3 getNormal() {
        if (normal == null)
            calculateNormal();
        return normal;
    }

    /** Sets the a-point from the given vector.
     * @param a The vector for the first vertex. */
    public void setA(final Vertex a) { this.a.set(a); }

    /** Sets the b-point from the given vector.
     * @param b The vector for the second vertex. */
    public void setB(final Vertex b) { this.b.set(b); }

    /** Sets the c-point from the given vector.
     * @param c The vector for the third vertex. */
    public void setC(final Vertex c) { this.c.set(c); }

    /** Sets the points of this triangle from the given vectors.
     * @param a The vector for the first vertex.
     * @param b The vector for the second vertex.
     * @param c The vector for the third vertex. */
    public void set(final Vertex a, final Vertex b, final Vertex c) {
        setA(a);
        setB(b);
        setC(c);
    }

    /** Sets the point of this triangle from given values
     * in the given position.
     * @param i The index of point. {@link Triangle#a} has 0-position,
     *          {@link Triangle#b} - 1, {@link Triangle#c} - 2.
     * @param x The x-component of this point.
     * @param y The y-component of this point.
     * @param z The z-component of this point. */
    public void set(int i, float x, float y, float z) {
        switch (i) {
            case 0:
                a.setPosition(new Vector3(x, y, z));
                break;
            case 1:
                b.setPosition(new Vector3(x, y, z));
                break;
            case 2:
                c.setPosition(new Vector3(x, y, z));
                break;
        }
    }

    /** Calculates the center of this triangle. */
    public void calculateCenter() {
        if (center == null)
            center = new Vector3();
        center.set(a.getPosition());
        center.add(b.getPosition()).add(c.getPosition()).div(3);
    }

    /** Calculates the normal vector of this triangle. */
    public void calculateNormal() {
        Vector3 firstSide = new Vector3(b.getPosition()).sub(a.getPosition());
        Vector3 secondSide = new Vector3(c.getPosition()).sub(a.getPosition());

        if (normal == null) {
            normal = new Vector3(firstSide);
        } else {
            normal.set(firstSide);
        }

        normal.cross(secondSide).normalize();
    }

    /** Sorts the vertexes of this triangle ascending y-coordinate. **/
    public void sortVertexes() {
        if (a.getPosition().y > b.getPosition().y) swap(a, b);
        if (a.getPosition().y > c.getPosition().y) swap(a, c);
        if (b.getPosition().y > c.getPosition().y) swap(b, c);
    }

    /* Swaps two vectors. */
    private void swap(Vertex a, Vertex b) {
        Vertex temp = a.copy();
        a.set(b);
        b.set(temp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (a != null ? !a.equals(triangle.a) : triangle.a != null) return false;
        if (b != null ? !b.equals(triangle.b) : triangle.b != null) return false;
        if (c != null ? !c.equals(triangle.c) : triangle.c != null) return false;
        if (center != null ? !center.equals(triangle.center) : triangle.center != null) return false;
        return !(normal != null ? !normal.equals(triangle.normal) : triangle.normal != null);

    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        result = 31 * result + (c != null ? c.hashCode() : 0);
        result = 31 * result + (center != null ? center.hashCode() : 0);
        result = 31 * result + (normal != null ? normal.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", center=" + center +
                ", normal=" + normal +
                '}';
    }
}
