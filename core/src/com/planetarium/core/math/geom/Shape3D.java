package com.planetarium.core.math.geom;

import com.planetarium.core.math.Vector3;

/**
 * Describes all shapes in 3D. Each shape has a center like vector.
 * @link com.planetarium.core.math.Vector3
 */
public abstract class Shape3D {
    /** The center of shape. **/
    protected Vector3 center;

    /** @return The center vector of this shape for changing. **/
    public Vector3 getCenter() { return center; }

    /** Sets the center of this shape from given vector.
     * @param center The center vector. */
    public void setCenter(final Vector3 center) {
        this.center.set(center);
    }

    /** Sets the center of this shape from given values.
     * @param x The x-component of center vector.
     * @param y The y-component of center vector.
     * @param z The z-component of center vector.     */
    public void setCenter(float x, float y, float z) {
        center.set(x, y, z);
    }

    /** @return A copy of this shape. **/
    public abstract Shape3D copy();
}
