package com.planetarium.core.math;

import java.util.Random;

/**
 * Class which contains methods for math operations with float precision.
 * Most methods are a wrapper of methods from {@link java.lang.Math}.
 */
public class FloatMath {

    /** The epsilon to "close to zero". It has 1e-6 precision. **/
    public static final float EPS = 0.0000001f;
    /** The value E as a float, the base of the natural logarithms. **/
    public static final float E = (float) Math.E;
    /** The value PI as a float. **/
    public static final float PI = (float) Math.PI;
    /** The value 2 PI as a float. **/
    public static final float DOUBLE_PI = PI * 2.0f;
    /** The value PI/2 as a float. **/
    public static final float HALF_PI = PI * 0.5f;
    /** The value PI/4 as a float. **/
    public static final float QUARTER_PI = PI * 0.25f;
    /** The value to convert a degrees to radians. **/
    public static final float DEGREE_TO_RADIAN = PI / 180.0f;
    /** The value to convert a radians to degrees. **/
    public static final float RADIAN_TO_DEGREE = 180.0f / PI;

    /** The generator of random numbers. **/
    public static final Random rand = new Random();

    /** Returns a random number between 0 and 1.
     * @see java.util.Random#nextInt()
     * @return The random number between 0 and 1. */
    public static int nextRandomInt() {
        return rand.nextInt();
    }

    /** Returns a random number between 0 and the given value.
     * @see java.util.Random#nextInt(int)
     * @param n The bound on the random number to be returned.
     *          Must be positive.
     * @return The random number between 0 and the given value. */
    public static int nextRandomInt(int n) {
        return rand.nextInt(n);
    }

    /** Returns a random number between a start and a end values.
     * @param start The start bound. It must be less then the end bound.
     * @param end The end bound. It must be more then the start bound.
     * @return The random number between the start and the end values. */
    public static int nextRandomInt(int start, int end) {
        if (end <= start) {
            throw new IllegalArgumentException("The end bound must be more" +
                    " then the start bound.");
        }
        return start + rand.nextInt(end - start + 1);
    }

    /** Returns a random number between 0.0 and 1.0.
     * @see Random#nextFloat()
     * @return The random number between 0.0 and 1.0. */
    public static float nextRandomFloat() {
        return rand.nextFloat();
    }

    /** Returns a random number between 0.0 and the given value.
     * @param range The bound of random values.
     * @return The random number between 0.0 and the given value. */
    public static float nextRandomFloat(float range) {
        return rand.nextFloat() * range;
    }

    /** Returns a random number between a start and a end values.
     * @param start The start bound.
     * @param end The end bound.
     * @return The random number between the start and the end values. */
    public static float nextRandomFloat(float start, float end) {
        return start + nextRandomFloat(end - start + 1);
    }

    /** Returns the trigonometric cosine of an angle.
     * @see java.lang.Math#cos(double)
     * @param value An angle in radians as a float.
     * @return The cosine of the argument as a float. */
    public static float cos(float value) {
        return (float) Math.cos(value);
    }

    /** Returns the trigonometric sine of an angle.
     * @see java.lang.Math#sin(double)
     * @param value An angle in radians as a float.
     * @return The sine of the argument as a float. */
    public static float sin(float value) {
        return (float) Math.sin(value);
    }

    /** Returns the trigonometric tangent of an angle.
     * @see java.lang.Math#tan(double)
     * @param value An angle in radians as a float.
     * @return The tangent of the argument as a float. */
    public static float tan(float value) {
        return (float) Math.tan(value);
    }

    /** Returns the arc cosine of a value.
     * @see java.lang.Math#acos(double)
     * @param value The value as a float whose arc cosine is to be returned.
     * @return The arc cosine of the argument as a float. */
    public static float acos(float value) {
        return (float) Math.acos(value);
    }

    /** Returns the arc sine of a value.
     * @see java.lang.Math#asin(double)
     * @param value The value as a float whose arc sine is to be returned.
     * @return The arc sine of the argument as a float. */
    public static float asin(float value) {
        return (float) Math.asin(value);
    }

    /** Returns the arc tangent of a value.
     * @see java.lang.Math#atan(double)
     * @param value The value as a float whose arc tangent is to be returned.
     * @return The arc tangent of the argument as a float. */
    public static float atan(float value) {
        return (float) Math.atan(value);
    }

    /** Returns the angle theta from the conversion of rectangular
     * coordinates (x, y) to polar coordinates (r, theta).
     * @see java.lang.Math#atan2(double, double)
     * @param x - The ordinate coordinate.
     * @param y - The abscissa coordinate.
     * @return the float value of the theta component of the point (r, theta)
     * in polar coordinates that corresponds to the point (x, y)
     * in Cartesian coordinates. */
    public static float atan2(float y, float x) {
        return (float) Math.atan2(y, x);
    }

    /** Returns Euler's number e raised to the power of a float value.
     * @see java.lang.Math#exp(double)
     * @param value The exponent to raise e to.
     * @return The value e^value. */
    public static float exp(float value) {
        return (float) Math.exp(value);
    }

    /** Returns the natural logarithm (base e) of a float value.
     * @see java.lang.Math#log(double)
     * @param value The value as a float.
     * @return The natural logarithm of the given value. */
    public static float log(float value) {
        return (float) Math.log(value);
    }

    /** Returns the base 10 logarithm of a float value.
     * @see java.lang.Math#log10(double)
     * @param value The value as a float.
     * @return The base 10 logarithm of value. */
    public static float log10(float value) {
        return (float) Math.log10(value);
    }

    /** Converts an angle measured in degrees to equivalent angle
     * measured in radians.
     * @param angdeg An angle in degrees.
     * @return The measurement of the given angle in radians. */
    public static float toRadians(float angdeg) {
        return angdeg * DEGREE_TO_RADIAN;
    }

    /** Converts an angle measured in radians to equivalent angle
     * measured in degrees.
     * @param angrad An angle in radians.
     * @return The measurement of the given angle in degrees.  */
    public static float toDegrees(float angrad) {
        return angrad * RADIAN_TO_DEGREE;
    }

    /** Returns the value of the first argument raised to the
     * power of the second argument.
     * @see java.lang.Math#pow(double, double)
     * @param a The base as a float.
     * @param b The exponent as a float.
     * @return The value a^b as a float. */
    public static float pow(float a, float b) {
        return (float) Math.pow(a, b);
    }

    /** Returns the closest int to the argument, with ties rounding up.
     * @see java.lang.Math#round(float)
     * @param value The value as a float.
     * @return The value of the argument rounded to the nearest int value. */
    public static int round(float value) {
        return Math.round(value);
    }

    /** Returns the absolute value of a float value.
     * @param value The value as a float.
     * @return The absolute value of the argument. */
    public static float abs(float value) {
        if (value < 0) {
            return -value;
        }
        return value;
    }

    /** Returns the value squared.
     * @param value The value to square.
     * @return The square of the given value. */
    public static float sqr(float value) {
        return value * value;
    }

    /** Returns the correctly rounded positive square root of a double value.
     * @see java.lang.Math#sqrt(double)
     * @param value The value as a float.
     * @return The positive square root of the given value. */
    public static float sqrt(float value) {
        return (float) Math.sqrt(value);
    }

    /** Compares a two numbers.
     * @see com.planetarium.core.math.FloatMath#EPS used like
     * precision for comparing.
     * @param a The first number.
     * @param b The second number.
     * @return True if a is nearly equal to b. */
    public static boolean isEqual(float a, float b) {
        return abs(a - b) <= EPS;
    }

    /** Compares a two numbers with the given epsilon.
     * @param a The first number.
     * @param b The second number.
     * @param epsilon The precision for compare.
     * @return True if a is nearly equal to b. */
    public static boolean isEqual(float a, float b, float epsilon) {
        return abs(a - b) <= epsilon;
    }

    /** Compares the given value and zero.
     * @see com.planetarium.core.math.FloatMath#EPS used like
     * precision for comparing.
     * @param value The number.
     * @return True if value is nearly equal to zero. */
    public static boolean isZero(float value) {
        return abs(value) <= EPS;
    }

    /** Compares the given value and zero with the given precision.
     * @param value The number.
     * @param epsilon The precision for comparing.
     * @return True if value is nearly equal to zero. */
    public static boolean isZero(float value, float epsilon) {
        return abs(value) <= epsilon;
    }

    /** Returns:
     *  1 - if the number is positive.
     *  -1 - if the number is negative.
     *  0 - if the number is zero.
     *  @param value The integer number.
     *  @return The sign of the given number. */
    public static int sign(int value) {
        if (value > 0) return 1;
        if (value < 0) return -1;
        return 0;
    }

    /** Returns:
     *  1 - if the number is positive.
     *  -1 - if the number is negative.
     *  0 - if the number is zero.
     *  @see java.lang.Math#signum(float)
     *  @param value The float number.
     *  @return The sign of the given number. */
    public static float sign(float value) {
        return Math.signum(value);
    }
}
