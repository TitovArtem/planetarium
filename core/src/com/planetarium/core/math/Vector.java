package com.planetarium.core.math;

/**
 * Interface which describe general operations and methods for
 * vectors in various dimensions of various types.
 */
public interface Vector<T extends Vector<T>> {
    /** @return A copy of this vector. */
    T copy();

    /** @return The euclidean length. **/
    float length();

    /** Sets this vector from given vector.
     * @param vector The vector.
     * @return This vector for changing. */
    T set(T vector);

    /** Subtracts the given vector from this vector.
     * @param vector The vector.
     * @return This vector for changing. */
    T sub(T vector);

    /** Adds the given vector to this vector.
     * @param vector The vector.
     * @return This vector for changing. */
    T add(T vector);

    /** Calculates the dot product between the given vector and this vector.
     * @param vector The vector.
     * @return The dot product between this and the given vector. */
    float dot(T vector);

    /** Calculates the cross product between this vector and the given vector.
     * It calculates only for 3D vector - {@link Vector3} and does nothing if
     * this vector has other dimension.
     * @param vector The vector.
     * @return The cross product between this and given vector. */
    T cross(T vector);

    /** Calculates distance between this vector and the given vector.
     * @param vector The vector.
     * @return The distance between this and given vector. */
    float distance(T vector);

    /** @return True if this vector is a zero vector. */
    boolean isZero();

    /** @param vector The vector.
     * @return True if this vector is collinear with the other vector. */
    boolean isCollinear(T vector);

    /** Sets all components of this vector to zero.
     * @return This vector for changing. */
    T setZero();

}
