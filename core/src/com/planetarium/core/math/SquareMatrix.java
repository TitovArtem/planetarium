package com.planetarium.core.math;

/** Interface which describe general operations and methods
 * for square matrices in various dimensions.
 * Implementation of matrix 4x4: {@link Matrix4}. */
public interface SquareMatrix<T extends SquareMatrix<T>> {

    /** @return A copy of this matrix. **/
    T copy();

    /** @return The dimension of this matrix. **/
    int getSize();

    /** @param i The index of row.
     * @param j The index of column.
     * @return Access element at [i, j]. */
    float get(int i, int j);

    /** @return The data of this matrix. **/
    float[][] get();

    /** Sets this matrix from given matrix.
     * @param other The square matrix.
     * @return This matrix for changing. */
    T set(T other);

    /** Sets this matrix from given array.
     * @param data The matrix.
     * @return This matrix for changing. */
    T set(float[][] data);

    /** Sets element of this matrix from given value.
     * @param i The index of row.
     * @param j The index of column.
     * @param value The number.
     * @return This matrix for changing. */
    T set(int i, int j, float value);

    /** Sets a row of this matrix from given array.
     * @param i The index of row.
     * @param row The array.
     * @return This matrix for changing. */
    T setRow(int i, float[] row);

    /** Sets a column of this matrix from given array.
     * @param j The index of column.
     * @param column The array.
     * @return This matrix for changing. */
    T setColumn(int j, float[] column);

    /** Subtracts the given matrix from this matrix.
     * @param other The square matrix.
     * @return This matrix for changing. */
    T sub(T other);

    /** Subtracts the given value from all elements of this matrix.
     * @param value The number.
     * @return This matrix for changing. */
    T sub(float value);

    /** Adds the given matrix to this matrix.
     * @param other The matrix.
     * @return This matrix for changing. */
    T add(T other);

    /** Adds the given value to all elements of this matrix.
     * @param value The number.
     * @return This matrix for changing. */
    T add(float value);

    /** Multiplies this matrix at the given matrix.
     * @param other The matrix.
     * @return This matrix for changing. */
    T multiple(T other);

    /** Multiplies all elements of this matrix at the given value.
     * @param value The number.
     * @return This matrix for changing. */
    T multiple(float value);

    /** Divides all elements of this matrix at the given value.
     * @param value The number.
     * @return This matrix for changing. */
    T div(float value);

    /** Transposes this matrix.
     * @return This matrix for changing.*/
    T transpose();
}
