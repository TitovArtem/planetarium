package com.planetarium.core.math;

/**
 * Describes the mathematical methods for vector which has
 * all components like integer type.
 */
public interface IntegerVector<T extends IntegerVector<T>> extends Vector<T> {

    /** Adds the given number to all components of this vector.
     * @param value The vector.
     * @return This vector for changing. */
    T add(int value);

    /** Subtracts the given number from all components of this vector.
     * @param value The number.
     * @return This vector for changing. */
    T sub(int value);

    /** Multiples all components of this vector at the given number.
     * @param value The number.
     * @return This vector for changing. */
    T multiple(int value);

    /** Sets all components of this vector to given value.
     * @param value The number.
     * @return This vector for changing. */
    T setComponentsLike(int value);
}
