package com.planetarium.core.math;

import java.io.Serializable;

/**
 * Class of mathematical 3D vector which has all components like float type.
 */
public class Vector2i implements IntegerVector<Vector2i>, Serializable {
    private static final long serialVersionUID = 1L;

    /** The x-component of this vector. **/
    public int x;
    /** The y-component of this vector. **/
    public int y;

    /**
     * Creates a new vector 2D from the given components.
     * @param x The x-component.
     * @param y The y-component.
     */
    public Vector2i(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Creates a new vector 2D from the given vector.
     * @param other Other vector 2D.
     */
    public Vector2i(final Vector2i other) {
        this.set(other);
    }

    /** Creates a new vector 2D from the given float coordinates. **/
    public Vector2i(float x, float y) {
        this.set(x, y);
    }

    /** Sets components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @return This vector for changing. */
    public Vector2i set(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    /** Sets components of this vector from the given vector
     * 3D with float components.
     * @param vector3 vector 3D with float components. */
    public Vector2i set(final Vector3 vector3) {
        return this.set(vector3.x, vector3.y);
    }

    /** Sets components of this vector from the given vector
     * 3D with float components. */
    public Vector2i set(float x, float y) {
        return this.set((int)x, (int)y);
    }

    @Override
    public Vector2i add(int value) {
        return this.set(x + value, y + value);
    }

    @Override
    public Vector2i sub(int value) {
        return this.add(-value);
    }

    @Override
    public Vector2i multiple(int value) {
        return this.set(x * value, y * value);
    }

    /** Multiplies all components of this vector at the given value.
     * @param value The value.
     * @return this vector for changing. */
    public Vector2i multiple(float value) {
        return this.set(x * value, y * value);
    }

    @Override
    public Vector2i setComponentsLike(int value) {
        return this.set(value, value);
    }

    @Override
    public Vector2i copy() {
        return new Vector2i(this);
    }

    @Override
    public float length() {
        return FloatMath.sqrt(x * x + y * y);
    }

    @Override
    public Vector2i set(final Vector2i vector) {
        return this.set(vector.x, vector.y);
    }

    @Override
    public Vector2i sub(final Vector2i vector) {
        return this.set(x - vector.x, y - vector.y);
    }

    @Override
    public Vector2i add(final Vector2i vector) {
        return this.set(x + vector.x, y + vector.y);
    }

    @Override
    public float dot(final Vector2i vector) {
        return x * vector.x + y * vector.y;
    }

    @Override
    public Vector2i cross(Vector2i vector) {
        return this;
    }

    @Override
    public float distance(Vector2i vector) {
        int a = x - vector.x;
        int b = y - vector.y;
        return FloatMath.sqrt(a * a + b * b);
    }

    @Override
    public boolean isZero() {
        return x == 0 && y == 0;
    }

    @Override
    public boolean isCollinear(Vector2i vector) {
        final float a = x / vector.x;
        final float b = y / vector.y;
        return FloatMath.isEqual(a, b);
    }

    @Override
    public Vector2i setZero() {
        return this.set(0, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector2i vector2i = (Vector2i) o;

        if (x != vector2i.x) return false;
        return y == vector2i.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "Vector2i{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
