package com.planetarium.core.math;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Class of square matrix 4x4.
 */
public class Matrix4 implements SquareMatrix<Matrix4>, Serializable {

    private static final long serialVersionUID = 1L;

    /** Dimension of this matrix (number of rows or columns). **/
    public static int SIZE = 4;

    /** The data of this matrix **/
    private float data[][];

    /** Get a zero matrix **/
    public static Matrix4 ZEROS = new Matrix4();

    /** Constructs a identity matrix (it's square matrix with ones
     * on the main diagonal and zeros elsewhere.
     * @return The identity matrix. */
    public static Matrix4 getIdentity() {
        Matrix4 matrix4 = new Matrix4();
        matrix4.data[0][0] = 1;
        matrix4.data[1][1] = 1;
        matrix4.data[2][2] = 1;
        matrix4.data[3][3] = 1;
        return matrix4;
    }

    /** Constructs a matrix from the given data.
     * @param data The data. */
    public Matrix4(final float[][] data) {
        this.data = new float[SIZE][SIZE];
        this.set(data);
    }

    /** Constructs a matrix from the given matrix.
     * @param matrix The matrix. */
    public Matrix4(final Matrix4 matrix) {
        data = new float[SIZE][SIZE];
        this.set(matrix);
    }

    /** Constructs a zero matrix. **/
    public Matrix4() {
        data = new float[SIZE][SIZE];
        this.set(0f);
    }

    @Override
    public Matrix4 copy() { return new Matrix4(this); }

    @Override
    public int getSize() { return SIZE; }

    @Override
    public float get(int i, int j) {
        checkBounds(i, j);
        return data[i][j];
    }

    /** @param i The index of row.
     * @return A row of this matrix. */
    public float[] getRow(int i) {
        if (i < 0 || i >= SIZE) {
            throw new ArrayIndexOutOfBoundsException("" +
                    "It's incorrect index. This row doesn't exist.");
        }
        return data[i];
    }

    @Override
    public float[][] get() { return data; }

    /** Sets all elements of this matrix from given value.
     * @param value The number.
     * @return This matrix for changing. */
    public Matrix4 set(float value) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] = value;
            }
        }
        return this;
    }

    @Override
    public Matrix4 set(final Matrix4 other) {
        return this.set(other.get());
    }

    @Override
    public Matrix4 set(final float[][] data) {
        if (data.length != SIZE || data[0].length != SIZE) {
            throw new IllegalArgumentException("Input data has incorrect dimension.");
        }

        // Copy the content of given matrix to data of this matrix
        for (int i = 0; i < SIZE; i++) {
            System.arraycopy(data[i], 0, this.data[i], 0, SIZE);
        }

        return this;
    }

    @Override
    public Matrix4 set(int i, int j, float value) {
        checkBounds(i, j);
        data[i][j] = value;
        return this;
    }

    @Override
    public Matrix4 setRow(int i, final float[] row) {
        if (i < 0 || i >= SIZE) {
            throw new ArrayIndexOutOfBoundsException("" +
                    "It's incorrect index. This row doesn't exist.");
        }
        System.arraycopy(row, 0, data[i], 0, SIZE);
        return this;
    }

    @Override
    public Matrix4 setColumn(int j, final float[] column) {
        if (j < 0 || j >= SIZE) {
            throw new ArrayIndexOutOfBoundsException("" +
                    "It's incorrect index. This column doesn't exist.");
        }

        // Copy the given array to column at the given index
        for (int i = 0; i < SIZE; i++) {
            data[i][j] = column[i];
        }

        return this;
    }

    @Override
    public Matrix4 sub(Matrix4 other) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] -= other.get(i, j);
            }
        }
        return this;
    }

    @Override
    public Matrix4 sub(float value) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] -= value;
            }
        }
        return this;
    }

    @Override
    public Matrix4 add(Matrix4 other) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] += other.get(i, j);
            }
        }
        return this;
    }

    @Override
    public Matrix4 add(float value) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] -= value;
            }
        }
        return this;
    }

    /** Multiplies the first matrix at the second matrix.
     * @param first The matrix.
     * @param second The matrix.
     * @return The new matrix for changing. */
    public static Matrix4 multiple(final Matrix4 first, final Matrix4 second) {
        Matrix4 result = new Matrix4();

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                for (int k = 0; k < SIZE; k++) {
                    result.data[i][j] += first.data[i][k] * second.data[k][j];
                }
            }
        }
        return result;
    }

    @Override
    public Matrix4 multiple(Matrix4 other) {
        return this.set(multiple(this, other));
    }

    @Override
    public Matrix4 multiple(float value) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] *= value;
            }
        }
        return this;
    }

    /** Multiplies this matrix at the given vector.
     * @param vector The vector 4D.
     * @return The given vector like result of multiply. */
    public Vector4 multiple(Vector4 vector) {
        return vector.set(Vector4.multiple(this, vector));
    }

    @Override
    public Matrix4 div(float value) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                data[i][j] /= value;
            }
        }
        return this;
    }

    @Override
    public Matrix4 transpose() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = i; j < SIZE; j++) {
                float temp = data[i][j];
                data[i][j] = data[j][i];
                data[j][i] = temp;
            }
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Matrix4 matrix4 = (Matrix4) o;

        return Arrays.deepEquals(data, matrix4.data);

    }

    @Override
    public int hashCode() {
        return data != null ? Arrays.deepHashCode(data) : 0;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Arrays.toString(data[0])).append('\n');
        stringBuffer.append(Arrays.toString(data[1])).append('\n');
        stringBuffer.append(Arrays.toString(data[2])).append('\n');
        stringBuffer.append(Arrays.toString(data[3])).append('\n');
        return "Matrix4{" +
                "data=\n" + stringBuffer.toString() +
                '}';
    }

    // Check bounds when somebody does access to element.
    private void checkBounds(int i, int j) {
        if (i < 0 || i >= SIZE || j < 0 || j >= SIZE) {
            throw new ArrayIndexOutOfBoundsException("" +
                    "It's incorrect indexes. This element doesn't exist.");
        }
    }
}
