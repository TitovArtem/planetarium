package com.planetarium.core.math;

import java.io.Serializable;

/**
 * Class of mathematical 3D vector which has all components like float type.
 */
public class Vector3 implements FloatVector<Vector3>, Serializable {

    private static final long serialVersionUID = 1L;

    /** The x-component of this vector. **/
    public float x;
    /** The y-component of this vector **/
    public float y;
    /** The z-component of this vector **/
    public float z;

    /** Get a vector of x axis - {1, 0, 0}. **/
    public final static Vector3 OX_VECTOR = new Vector3(1, 0, 0);
    /** Get a vector of y axis - {0, 1, 0}. **/
    public final static Vector3 OY_VECTOR = new Vector3(0, 1, 0);
    /** Get a vector of z axis - {0, 0, 1}. **/
    public final static Vector3 OZ_VECTOR = new Vector3(0, 0, 1);
    /** Get a zero vector - {0, 0, 0}. **/
    public final static Vector3 ZERO_VECTOR = new Vector3(0, 0, 0);
    /** Get a vector which have all components like one - {1, 1, 1}. **/
    public final static Vector3 ONE_VECTOR = new Vector3(1, 1, 1);

    /** Constructs a vector with the given components.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component. */
    public Vector3(float x, float y, float z) {
        this.set(x, y, z);
    }

    /** Constructs a vector from the given vector.
     * @param vector The vector. */
    public Vector3(final Vector3 vector) { this.set(vector); }

    /** Constructs a vector 3D from the given vector 4D.
     * It works so: (x, y, z, w) -> (x, y, z).
     * @param vector The vector 4D. */
    public Vector3(final Vector4 vector) {
        set(vector.x, vector.y, vector.z);
    }

    /** Constructs a zero vector **/
    public Vector3() { this.set(0, 0, 0); }

    @Override
    public Vector3 copy() { return new Vector3(this); }

    @Override
    public float length() {
        return (float)Math.sqrt(x * x + y * y + z * z);
    }

    /** Sets the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @return This vector for changing. */
    public Vector3 set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    @Override
    public Vector3 set(final Vector3 vector) {
        return this.set(vector.x, vector.y, vector.z);
    }

    @Override
    public Vector3 sub(final Vector3 vector) {
        return this.set(x - vector.x, y - vector.y, z - vector.z);
    }

    @Override
    public Vector3 sub(float value) {
        return this.set(x + value, y + value, z + value);
    }

    /** Subtracts the given values from the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @return This vector for changing.
     */
    public Vector3 sub(float x, float y, float z) {
        return this.set(this.x - x, this.y - y, this.z - z);
    }

    @Override
    public Vector3 normalize() {
        float len = this.length();
        if (len == 0f || len == 1f) return this;
        return this.div(len);
    }

    /** Adds the given values to the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @return This vector for changing. */
    public Vector3 add(float x, float y, float z) {
        return this.set(this.x + x, this.y + y, this.z + z);
    }

    @Override
    public Vector3 add(final Vector3 vector) {
        return this.add(vector.x, vector.y, vector.z);
    }

    @Override
    public Vector3 add(float value) {
        return this.set(value, value, value);
    }


    @Override
    public float dot(final Vector3 vector) {
        return (x * vector.x + y * vector.y + z * vector.z);
    }

    @Override
    public Vector3 cross(final Vector3 vector) {
        float x = this.y * vector.z - this.z * vector.y;
        float y = this.z * vector.x - this.x * vector.z;
        float z = this.x * vector.y - this.y * vector.x;
        return this.set(x, y, z);
    }

    @Override
    public Vector3 multiple(float value) {
        return this.set(x * value, y * value, z * value);
    }

    @Override
    public Vector3 div(float value) {
        return this.set(x / value, y / value, z / value);
    }

    @Override
    public float distance(final Vector3 vector) {
        final float x = vector.x - this.x;
        final float y = vector.y - this.y;
        final float z = vector.z - this.z;
        return (float)Math.sqrt(x * x + y * y + z * z);
    }

    @Override
    public boolean isZero() {
        return FloatMath.isZero(x) && FloatMath.isZero(y) && FloatMath.isZero(z);
    }

    @Override
    public boolean isCollinear(final Vector3 vector) {
        final float a = x / vector.x;
        final float b = y / vector.y;
        final float c = z / vector.z;
        return FloatMath.isEqual(a, b) && FloatMath.isEqual(b, c);
    }

    @Override
    public Vector3 setZero() {
        return this.set(0f, 0f, 0f);
    }

    @Override
    public Vector3 setComponentsLike(float value) {
        return this.set(value, value, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3 vector3 = (Vector3) o;

        if (Float.compare(vector3.x, x) != 0) return false;
        if (Float.compare(vector3.y, y) != 0) return false;
        return Float.compare(vector3.z, z) == 0;

    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Vector3{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
