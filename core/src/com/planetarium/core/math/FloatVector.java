package com.planetarium.core.math;

/**
 * Describes math methods for vector which has all components like float type.
 * Implementation of 3D vector: {@link Vector3}.
 * Implementation of 4D vector: {@link Vector4}.
 */
public interface FloatVector<T extends FloatVector<T>> extends Vector<T> {

    /** Adds the given number to all components of this vector.
     * @param value The vector.
     * @return This vector for changing. */
    T add(float value);

    /** Subtracts the given number from all components of this vector.
     * @param value The number.
     * @return This vector for changing. */
    T sub(float value);

    /** Multiples all components of this vector at the given number.
     * @param value The number.
     * @return This vector for changing. */
    T multiple(float value);

    /** Divides all components of this vector at the given number.
     * @param value The number.
     * @return This vector for changing. */
    T div(float value);

    /** Sets all components of this vector to given value.
     * @param value The number.
     * @return This vector for changing. */
    T setComponentsLike(float value);

    /** Normalizes this vector.
     * Does nothing if this vector is zero.
     * @return This vector for changing. */
    T normalize();
}
