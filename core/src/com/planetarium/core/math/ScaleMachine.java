package com.planetarium.core.math;

import com.planetarium.core.math.geom.Shape3D;

/**
 * Allows to scale parameters of space objects.
 * It would be useful for scaling space objects to screen format.
 * If you want to make a demonstrative models you can use it.
 */
public interface ScaleMachine {

    /** Scales the given shape's parameters.
     * @param shape A shape for scaling.
     * @return A scaled shape. */
    Shape3D scale(Shape3D shape);
}
