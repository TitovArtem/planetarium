package com.planetarium.core.math;

import java.io.Serializable;

/**
 * Encapsulates everything what are needed to describe a vertex.
 * It includes the position of vertex, the normal to it
 * and coordinates of texture.
 */
public class Vertex implements Serializable {
    private static final long serialVersionUID = 1l;

    /** The position of this vertex. **/
    private Vector3 position;
    /** The normal to this vertex. **/
    private Vector3 normal;
    /** The texture coordinates. **/
    private Vector2i texel;

    /** Creates a new vertex from the given position.
     * @param position The position of new vertex. */
    public Vertex(final Vector3 position) {
        this.position = new Vector3(position);
    }

    /** Creates a new vertex from the given vertex.
     * @param other Other vertex. */
    public Vertex(final Vertex other) {
        position = new Vector3();
        this.set(other);
    }

    /** @return A copy of this vertex. **/
    public Vertex copy() {
        return new Vertex(this);
    }

    /** Sets this vertex from the given vertex.
     * @param other Other vertex. */
    public void set(final Vertex other) {
        position.set(other.position);
        if (other.normal != null) {
            setNormal(other.normal);
        }
        if (other.texel != null) {
            setTexel(other.texel);
        }
    }

    /** @return The position of this vertex for changing. **/
    public Vector3 getPosition() {
        return position;
    }

    /** Sets the position of this vertex from the given vector.
     * @param position Vector 3D for position. */
    public void setPosition(final Vector3 position) {
        this.position.set(position);
    }

    /** @return The normal to this vertex for changing. */
    public Vector3 getNormal() {
        return normal;
    }

    /** Sets normal to this vertex from the given vertex.
     * @param normal Vector 3D for normal to this vertex. */
    public void setNormal(final Vector3 normal) {
        if (this.normal == null) {
            this.normal = new Vector3(normal);
        } else {
            this.normal.set(normal);
        }
    }

    /** @return The vector with texture coordinates for this vertex for changing. **/
    public Vector2i getTexel() {
        return texel;
    }

    /** Sets vector with texture coordinates for this vertex.
     * @param texel The 2D vector with texture coordinates. */
    public void setTexel(final Vector2i texel) {
        if (this.texel == null) {
            this.texel = new Vector2i(texel);
        } else {
            this.texel.set(texel);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        if (position != null ? !position.equals(vertex.position) : vertex.position != null) return false;
        if (normal != null ? !normal.equals(vertex.normal) : vertex.normal != null) return false;
        return !(texel != null ? !texel.equals(vertex.texel) : vertex.texel != null);

    }

    @Override
    public int hashCode() {
        int result = position != null ? position.hashCode() : 0;
        result = 31 * result + (normal != null ? normal.hashCode() : 0);
        result = 31 * result + (texel != null ? texel.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "position=" + position +
                ", normal=" + normal +
                ", texel=" + texel +
                '}';
    }
}
