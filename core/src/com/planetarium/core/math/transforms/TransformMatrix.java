package com.planetarium.core.math.transforms;

import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Matrix4;
import com.planetarium.core.math.Vector3;

/**
 * Contains the static methods to create transform matrices for
 * rotating, scaling and moving objects.
 */
public class TransformMatrix {

    /** Creates the transform matrix for moving.
     * @param x Displacement along the x-axis.
     * @param y Displacement along the y-axis.
     * @param z Displacement along the z-axis.
     * @return The transform matrix for moving. */
    public static Matrix4 createMoveMatrix(float x, float y, float z) {
        Matrix4 matrix = Matrix4.getIdentity();
        matrix.set(0, 3, x);
        matrix.set(1, 3, y);
        matrix.set(2, 3, z);
        return matrix;
    }

    /** Creates the transform matrix for moving.
     * @param offset Vector with coordinates for displacement.
     * @return The transform matrix for moving. */
    public static Matrix4 createMoveMatrix(final Vector3 offset) {
        return createMoveMatrix(offset.x, offset.y, offset.z);
    }

    /** Creates the transform matrix for scaling.
     * @param x Scale factor along the x-axis.
     * @param y Scale factor along the y-axis.
     * @param z Scale factor along the z-axis.
     * @return The transform matrix for scaling. */
    public static Matrix4 createScaleMatrix(float x, float y, float z) {
        Matrix4 matrix = Matrix4.getIdentity();
        matrix.set(0, 0, x);
        matrix.set(1, 1, y);
        matrix.set(2, 2, z);
        return matrix;
    }

    /** Creates the transform matrix for scaling.
     * @param factors Vector with scale factors.
     * @return The transform matrix for scaling. */
    public static Matrix4 createScaleMatrix(final Vector3 factors) {
        return createScaleMatrix(factors.x, factors.y, factors.z);
    }

    /** Creates the transform matrix for rotating.
     * @param a The angle of rotation, in degrees.
     * @param x The x-component of vector.
     * @param y The y-component of vector.
     * @param z The z-component of vector.
     * @return The transform matrix for rotating. */
    public static Matrix4 createRotateMatrix(float a, float x, float y, float z) {
        float s = FloatMath.sin(a);
        float c = FloatMath.cos(a);
        float t = 1.0f - c;

        float tx = t * x;
        float ty = t * y;
        float tz = t * z;

        float sz = s * z;
        float sy = s * y;
        float sx = s * x;

        Matrix4 matrix = Matrix4.getIdentity();
        matrix.set(0, 0, tx * x + c);
        matrix.set(0, 1, tx * y + sz);
        matrix.set(0, 2, tx * z - sy);
        matrix.set(0, 3, 0f);

        matrix.set(1, 0, tx * y - sz);
        matrix.set(1, 1, ty * y + c);
        matrix.set(1, 2, ty * z + sx);
        matrix.set(1, 3, 0f);

        matrix.set(2, 0, tx * z + sy);
        matrix.set(2, 1, ty * z - sx);
        matrix.set(2, 2, tz * z + c);
        matrix.set(2, 3, 0f);

        return matrix;
    }

    /** Creates the transform matrix for rotating.
     * @param vector The vector around which the object is rotated.
     * @param a The angle of rotation, in degrees.
     * @return The transform matrix for rotating. */
    public static Matrix4 createRotateMatrix(float a, final Vector3 vector) {
        return createRotateMatrix(a, vector.x, vector.y, vector.z);
    }
}