package com.planetarium.core.math.transforms;

import com.planetarium.core.math.Matrix4;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.Vector4;

/**
 * Describes a transformation of vectors by transform matrices.
 */
public class Transformer {

    /** Transforms the given vertexes by the given transform matrix.
     * @param vertexes The array of vertexes for transforming.
     * @param matrix The transform matrix. */
    public static void transform(Vector3[] vertexes, final Matrix4 matrix) {
        Vector4[] result = supplementVectors(vertexes);
        for (int i = 0; i < vertexes.length; i++) {
            matrix.multiple(result[i]);
        }

        for (int i = 0; i < vertexes.length; i++) {
            vertexes[i].set(result[i].x, result[i].y, result[i].z);
        }
    }

    /** Transforms the given vector by the given transform matrix.
     * @param vector The vector for transforming.
     * @param matrix The transform matrix. */
    public static void transform(Vector3 vector, final Matrix4 matrix) {
        Vector4 vector4 = new Vector4(vector);
        matrix.multiple(vector4);
        vector.set(vector4.x, vector4.y, vector4.z);
    }

    /** Gets a new array of transformed vertexes from the given
     * vertexes and the given transform matrix.
     * @param vertexes The array of vertexes.
     * @param matrix The transform matrix.
     * @return The array of vertexes (vectors 3D). */
    public static Vector3[] getTransformedVertexes(final Vector3[] vertexes,
                                                  final Matrix4 matrix)
    {
        Vector4[] result = supplementVectors(vertexes);
        for (int i = 0; i < vertexes.length; i++) {
            matrix.multiple(result[i]);
        }

        return curtailVectors(result);
    }

    /** Gets a new array of transformed vectors 4D from the given
     * vertexes and the given transform matrix.
     * @param vertexes The array of vertexes.
     * @param matrix The transform matrix.
     * @return  The array of vectors 4D. */
    public static Vector4[] getTransformedVectors(final Vector3[] vertexes,
                                                  final Matrix4 matrix)
    {
        Vector4[] result = supplementVectors(vertexes);
        for (int i = 0; i < vertexes.length; i++) {
            matrix.multiple(result[i]);
        }

        return result;
    }

    /** Makes array of vectors 3D from vectors 4D.
     * It works so: (x, y, z, w) -> (x, y, z).
     * @param vectors The array of vectors 4D (x, y, z, w).
     * @return The array of vectors 3D (x, y, z). */
    public static Vector3[] curtailVectors(final Vector4[] vectors) {
        Vector3[] result = new Vector3[vectors.length];
        for (int i = 0; i < vectors.length; i++) {
            result[i] = new Vector3(vectors[i]);
        }
        return result;
    }

    /** Makes array of vectors 3D from vectors 4D like this:
     * (x, y, z, w) -> (x / w, y / w, z/ w).
     * @param vectors The array of vectors 4D.
     * @return The array of converted vectors 3D. */
    public static Vector3[] projectionConvert(final Vector4[] vectors) {
        Vector3[] result = new Vector3[vectors.length];
        for (int i = 0; i < vectors.length; i++) {
            float w = vectors[i].w;
            result[i] = new Vector3(vectors[i].x / w,
                    vectors[i].y / w,
                    vectors[i].z / w);
        }
        return result;
    }

    /** Sets elements of vectors 3D from vectors 4D like this:
     * (x, y, z, w) -> (x / w, y / w, z/ w).
     * @param vectors The array of vectors 4D.
     * @return The array of converted vectors 3D. */
    public static void projectionConvert(Vector3[] vectors3, final Vector4[] vectors) {
        for (int i = 0; i < vectors.length; i++) {
            float w = vectors[i].w;
            vectors3[i].set(vectors[i].x / w, vectors[i].y / w, vectors[i].z / w);
        }
    }

    /** Sets the first three values from vectors 4D to array of vectors 3D .
     * It works so: (x, y, z, w) -> (x, y, z).
     * @param vectors The array of vectors 4D (x, y, z, w).
     * @param vectors3  The array of vectors 3D (x, y, z). */
    public static void curtailVectors(Vector3[] vectors3, final Vector4[] vectors) {
        for (int i = 0; i < vectors.length; i++) {
            vectors3[i].set(vectors[i].x, vectors[i].y, vectors[i].z);
        }
    }

    /** Makes array of vectors 4D from vectors 3D.
     * It works so: (x, y, z) -> (x, y, z, 1).
     * @param vectors The array of vectors 3D (x, y, z).
     * @return The array of vectors 4D (x, y, z, 1). */
    public static Vector4[] supplementVectors(final Vector3[] vectors) {
        Vector4[] result = new Vector4[vectors.length];
        for (int i = 0; i < vectors.length; i++) {
            result[i] = new Vector4(vectors[i]);
        }
        return result;
    }
}
