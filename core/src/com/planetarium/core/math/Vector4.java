package com.planetarium.core.math;

import java.io.Serializable;

/**
 * Class of mathematical 4D vector which has all components like float type.
 */
public class Vector4 implements FloatVector<Vector4>, Serializable {

    private static final long serialVersionUID = 1L;

    /** The x-component of this vector **/
    public float x;
    /** The y-component of this vector **/
    public float y;
    /** The z-component of this vector **/
    public float z;
    /** The w-component of this vector **/
    public float w;

    /** Get a vector of x axis - {1, 0, 0, 0}. **/
    public static Vector4 OX_VECTOR = new Vector4(1, 0, 0, 0);
    /** Get a vector of y axis - {0, 1, 0, 0}. **/
    public static Vector4 OY_VECTOR = new Vector4(0, 1, 0, 0);
    /** Get a vector of z axis - {0, 0, 1, 0}. **/
    public static Vector4 OZ_VECTOR = new Vector4(0, 0, 1, 0);
    /** Get a vector of w axis - {0, 0, 0, 1}. **/
    public static Vector4 OW_VECTOR = new Vector4(0, 0, 0, 1);
    /** Get a zero vector - {0, 0, 0, 0}. **/
    public static Vector4 ZERO_VECTOR = new Vector4(0, 0, 0, 0);
    /** Get a vector which have all components like one - {1, 1, 1, 1}. */
    public static Vector4 ONE_VECTOR = new Vector4(1, 1, 1, 1);

    /** Constructs a vector with the given components.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @param w The w-component. */
    public Vector4(float x, float y, float z, float w) {
        this.set(x, y, z, w);
    }

    /** Constructs a vector from the given vector.
     * @param vector The vector. */
    public Vector4(final Vector4 vector) { this.set(vector); }

    /** Constructs a zero vector. **/
    public Vector4() { this.setZero(); }

    /** Constructs a vector from the given 3D vector where w-component = 1.
     * @param vector The 3D vector: {@link Vector3}. */
    public Vector4(final Vector3 vector) {
        this.set(vector.x, vector.y, vector.z, 1f);
    }

    @Override
    public Vector4 copy() {
        return new Vector4(this);
    }

    @Override
    public float length() {
        return (float)Math.sqrt(x * x + y * y + z * z + w * w);
    }

    /** Sets the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @param w The w-component.
     * @return This vector for changing. */
    public Vector4 set(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        return this;
    }

    @Override
    public Vector4 set(final Vector4 vector) {
        return this.set(vector.x, vector.y, vector.z, vector.w);
    }

    /** Subtracts the given values from the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @param w The w-component.
     * @return This vector for changing. */
    public Vector4 sub(float x, float y, float z, float w) {
        return this.set(this.x - x, this.y - y, this.z - z, this.w - w);
    }

    @Override
    public Vector4 sub(final Vector4 vector) {
        return this.sub(vector.x, vector.y, vector.z, vector.w);
    }

    @Override
    public Vector4 sub(float value) {
        return this.sub(value, value, value, value);
    }

    @Override
    public Vector4 normalize() {
        float len = this.length();
        if (len == 0f || len == 1f) return this;
        return this.div(len);
    }

    /** Adds the given values to the components of this vector.
     * @param x The x-component.
     * @param y The y-component.
     * @param z The z-component.
     * @param w The w-component.
     * @return This vector for changing. */
    public Vector4 add(float x, float y, float z, float w) {
        return this.set(this.x + x, this.y + y, this.z + z, this.w + w);
    }

    @Override
    public Vector4 add(final Vector4 vector) {
        return this.add(vector.x, vector.y, vector.z, vector.w);
    }

    @Override
    public Vector4 add(float value) {
        return this.add(value, value, value, value);
    }

    @Override
    public float dot(final Vector4 vector) {
        return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
    }

    @Override
    public Vector4 cross(final Vector4 vector) {
        return this;
    }

    @Override
    public Vector4 multiple(float value) {
        return this.set(x * value, y * value, z * value, w * value);
    }

    /** Multiplies the vector (like row) at the matrix.
     * @param vector The vector.
     * @param matrix The matrix. {@link Matrix4}
     * @return The vector. */
    public static Vector4 multiple(final Vector4 vector, final Matrix4 matrix) {
        Vector4 result = new Vector4();

        for (int i = 0; i < 4; i++) {
            float s = 0;
            s += vector.x * matrix.get(0, i);
            s += vector.y * matrix.get(1, i);
            s += vector.z * matrix.get(2, i);
            s += vector.w * matrix.get(3, i);
            switch (i) {
                case 0:
                    result.x = s;
                    break;
                case 1:
                    result.y = s;
                    break;
                case 2:
                    result.z = s;
                    break;
                case 3:
                    result.w = s;
                    break;
            }
        }
        return result;
    }

    /** Multiplies the matrix at the vector (like column).
     * @param vector The vector.
     * @param matrix The matrix. {@link Matrix4}
     * @return The vector. */
    public static Vector4 multiple(final Matrix4 matrix, final Vector4 vector) {
        Vector4 result = new Vector4();

        for (int i = 0; i < 4; i++) {
            float s = 0;
            s += vector.x * matrix.get(i, 0);
            s += vector.y * matrix.get(i, 1);
            s += vector.z * matrix.get(i, 2);
            s += vector.w * matrix.get(i, 3);
            switch (i) {
                case 0:
                    result.x = s;
                    break;
                case 1:
                    result.y = s;
                    break;
                case 2:
                    result.z = s;
                    break;
                case 3:
                    result.w = s;
                    break;
            }
        }
        return result;
    }

    /** Multiplies this vector at the given matrix.
     * @param matrix The matrix. {@link Matrix4}
     * @return This vector for changing. */
    public Vector4 multiple(final Matrix4 matrix) {
        return this.set(multiple(this, matrix));
    }

    @Override
    public Vector4 div(float value) {
        return this.set(x / value, y / value, z / value, w / value);
    }

    @Override
    public float distance(final Vector4 vector) {
        final float a = vector.x - x;
        final float b = vector.y - y;
        final float c = vector.z - z;
        final float d = vector.w - w;
        return (float)Math.sqrt(a * a + b * b + c * c + d * d);
    }

    @Override
    public boolean isZero() {
        return FloatMath.isZero(x) && FloatMath.isZero(y)
                && FloatMath.isZero(z) && FloatMath.isZero(w);
    }

    @Override
    public boolean isCollinear(final Vector4 vector) {
        final float a = x / vector.x;
        final float b = y / vector.y;
        final float c = z / vector.z;
        final float d = w / vector.w;
        return FloatMath.isEqual(a, b)
                && FloatMath.isEqual(b, c)
                && FloatMath.isEqual(c, d);
    }

    @Override
    public Vector4 setZero() {
        return this.set(0, 0, 0, 0);
    }

    @Override
    public Vector4 setComponentsLike(float value) {
        return this.set(value, value, value, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector4 vector4 = (Vector4) o;

        if (Float.compare(vector4.x, x) != 0) return false;
        if (Float.compare(vector4.y, y) != 0) return false;
        if (Float.compare(vector4.z, z) != 0) return false;
        return Float.compare(vector4.w, w) == 0;

    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        result = 31 * result + (w != +0.0f ? Float.floatToIntBits(w) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Vector4{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", w=" + w +
                '}';
    }
}
