package com.planetarium.core.physics.orbit;

import com.planetarium.core.math.Vector3;
import com.planetarium.core.objects.SpaceObject;

/**
 * Interface which describes a orbital movement of space objects.
 * It has the method which allows to calculate a new position
 * of object in space. The time is a measure that allows to calculate
 * how far to move object. It can be: hours, days, years and other values. */
public interface OrbitEngine {

    /** Sets the given space object for this engine.
     * @param object A space object which will be moved. */
    void set(SpaceObject object);

    /** Moves the given space object. It should change orbit of the given
     * object and sets a new coordinates of center.
     * @param value A value describes the time (typically it's solar days).
     * @param spaceObject A space object for changing. */
    void move(SpaceObject spaceObject, double value);

    /** Calculates the new position of object.
     * It shouldn't change object, it only have to calculate
     * a new coordinates of center.
     * @param value A value describes the time (typically it's solar days).
     * @return The new position of center. */
    Vector3 moveAt(double value);

    /** @return A copy of this object engine. **/
    OrbitEngine copy();
}
