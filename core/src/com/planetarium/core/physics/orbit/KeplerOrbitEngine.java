package com.planetarium.core.physics.orbit;

import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.objects.SpaceObject;

import static java.lang.Math.*;

/**
 * Describes a movement of object at the orbit by Kepler's laws.
 */
public class KeplerOrbitEngine implements OrbitEngine {
    /** The orbit of this space object. **/
    private Orbit orbit;
    /** Number of days in the year. **/
    public static final double DAYS_PER_YEAR = 365.256898326;

    /** Constructs a new engine and copies orbit from the given object.
     * @param object The space object. */
    public KeplerOrbitEngine(final SpaceObject object) {
        set(object);
    }

    /** Constructs a new engine from the given orbit.
     * @param object The orbit of space object. */
    public KeplerOrbitEngine(final Orbit object) {
        set(object);
    }

    /** Sets the orbit from the given space object (like a copy).
     * @param object The space object. */
    public void set(final SpaceObject object) {
        orbit = object.getOrbit().copy();
        convertToRadians();
    }

    /** Sets the orbit.
     * @param orbit The orbit for engine. */
    public void set(final Orbit orbit) {
        this.orbit = orbit.copy();
    }

    /** Converts the attributes of this orbit (only omega, l, m, i) to radians. **/
    private void convertToRadians() {
        orbit.setInclination(orbit.getInclination() * FloatMath.DEGREE_TO_RADIAN);
        orbit.setLongitude(orbit.getLongitude() * FloatMath.DEGREE_TO_RADIAN);
        orbit.setMeanAnomaly(orbit.getMeanAnomaly() * FloatMath.DEGREE_TO_RADIAN);
        orbit.setEccentricity(orbit.getEccentricity() * FloatMath.DEGREE_TO_RADIAN);
        orbit.setPeriapsisArgument(orbit.getPeriapsisArgument()
                * FloatMath.DEGREE_TO_RADIAN);
    }

    /** @return The value of daily motion. It states how far in degrees
     * the planet moves in one (mean solar) day. */
    public double calculateDailyMotion() {
        double n = 360.0 / (DAYS_PER_YEAR * pow(orbit.getSemimajorAxis(), 1.5));
        if (Double.isInfinite(n)) n = 0.0;
        return n * FloatMath.DEGREE_TO_RADIAN;
    }

    /** Calculates the eccentric anomaly from the mean anomaly
     * with the default precision (10^e-5). It has the bound
     * of iterations = 50.
     * @param m The mean anomaly.
     * @return Th value of eccentric anomaly.  */
    public double calculateEccentricAnomaly(double m) {
        double eps = 1e-5;
        int maxIterations = 50;
        double e = orbit.getEccentricity();
        double e0 = m + e * (180 / PI) * sin(m) * (1.0 + e * cos(m));
        double e1 = e0 - (e0 - e * (180/ PI) * sin(e0) - m) / (1 - e * cos(e0));
        int counter = 0;
        while (abs(e - e0) > eps && counter++ > maxIterations) {
            e0 = e1;
            e1 = e0 - (e0 - e * (180/ PI) * sin(e0) - m) / (1 - e * cos(e0));
        }
        return e1;
    }

    /** Calculates the eccentric anomaly from the mean anomaly
     * with the given precision. Warning: it doesn't has max bound
     * of iterations!
     * @param m The mean anomaly.
     * @param eps The precision for calculating.
     * @return Th value of eccentric anomaly. */
    public double calculateEccentricAnomaly(double m, double eps) {
        double e = orbit.getEccentricity();
        double e0 = m + e * (180 / PI) * sin(m) * (1.0 + e * cos(m));
        double e1 = e0 - (e0 - e * (180/ PI) * sin(e0) - m) / (1 - e * cos(e0));
        while (abs(e - e0) > eps) {
            e0 = e1;
            e1 = e0 - (e0 - e * (180/ PI) * sin(e0) - m) / (1 - e * cos(e0));
        }
        return e1;
    }

    /** Calculates the coordinates of object at the new position.
     * @param days The number of days since the date of the elements.
     * @return The vector with new coordinates. */
    @Override
    public Vector3 moveAt(double days) {
        double m = orbit.getMeanAnomaly() + calculateDailyMotion() * days;
        double E = calculateEccentricAnomaly(m);
        double a = orbit.getSemimajorAxis();
        double e = orbit.getEccentricity();

        double xv = a * (cos(E) - e);
        double yv = a * (sqrt(1.0 - e * e) * sin(E));

        double v = atan2(yv, xv);
        double r = sqrt(xv * xv + yv * yv);
        double i = orbit.getInclination();
        double N = orbit.getLongitude();
        double w = orbit.getPeriapsisArgument();

        double x = r * (cos(N) * cos(v + w) - sin(N) * sin(v + w) * cos(i));
        double y = r * (sin(N) * cos(v+w) + cos(N) * sin(v+w) * cos(i));
        double z = r * (sin(v + w) * sin(i));

        return new Vector3((float)x, (float)y, (float)z);
    }


    /** Move the given object at the given days.
     * It changes the orbit of this engine too!
     * @param days The number of days.
     * @param spaceObject The space object for movement. */
    @Override
    public void move(SpaceObject spaceObject, double days) {
        if (!spaceObject.getOrbit().equals(orbit)) {
            set(spaceObject);
        }

        double m = orbit.getMeanAnomaly() + calculateDailyMotion() * days;
        double E = calculateEccentricAnomaly(m);
        double a = orbit.getSemimajorAxis();
        double e = orbit.getEccentricity();

        double xv = a * (cos(E) - e);
        double yv = a * (sqrt(1.0 - e * e) * sin(E));

        double v = atan2(yv, xv);//calculateTrueAnomaly(m);
        double r = sqrt(xv * xv + yv * yv);//calculateRadiusVector(v);
        double i = orbit.getInclination();
        double N = orbit.getLongitude();
        double w = orbit.getPeriapsisArgument();

        double x = r * (cos(N) * cos(v + w) - sin(N) * sin(v + w) * cos(i));
        double y = r * (sin(N) * cos(v+w) + cos(N) * sin(v+w) * cos(i));
        double z = r * (sin(v + w) * sin(i));

        orbit.setMeanAnomaly(m);
        spaceObject.getOrbit().setMeanAnomaly(m * FloatMath.RADIAN_TO_DEGREE);
        spaceObject.setCenter((float)x, (float)y, (float)z);
    }

    @Override
    public OrbitEngine copy() {
        return new KeplerOrbitEngine(orbit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeplerOrbitEngine that = (KeplerOrbitEngine) o;

        return !(orbit != null ? !orbit.equals(that.orbit) : that.orbit != null);
    }

    @Override
    public int hashCode() {
        return orbit != null ? orbit.hashCode() : 0;
    }
}
