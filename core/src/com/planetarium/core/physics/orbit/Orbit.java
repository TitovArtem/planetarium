package com.planetarium.core.physics.orbit;

import java.io.Serializable;

/**
 * Describes Keplerian orbital elements.
 * @link https://en.wikipedia.org/wiki/Kepler_orbit
 */
public class Orbit implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The semi-major axis. It's the half the greatest width
     * of the orbital ellipse, which gives the size of the orbit. **/
    private double a;

    /** The eccentricity. It's a number, giving the
     * shape of the orbit. For a circle e = 0, larger values give
     * progressively more flattened circles, up to e = 1 where
     * the ellipse stretches to infinity and becomes a parabola. **/
    private double e;

    /** The inclination. It's the angle between the orbital plane
     *  and the reference plane. */
    private double i;

    /** The longitude of the ascending node. It's the angle between
     * the reference direction and the upward crossing of
     * the orbit on the reference plane (the ascending node). */
    private double l;

    /** The argument of periapsis. It's the angle between the
     *  ascending node and the periapsis. It can range from 0 to 360.*/
    private double omega;

    /** The mean anomaly. It's an angle growing at a steady rate,
     *  increasing by 360 degrees each orbit. */
    private double m;

    /** Creates a new orbit from the given values.
     * @param a Semi-major axis.
     * @param e Eccentricity.
     * @param i Inclination.
     * @param l Longitude of the ascending node.
     * @param omega Argument of periapsis.
     * @param m Mean anomaly. */
    public Orbit(double a, double e, double i, double l, double omega, double m) {
        this.a = a;
        this.e = e;
        this.i = i;
        this.l = l;
        this.omega = omega;
        this.m = m;
    }

    /** Creates a new orbit with null values of fields. **/
    public Orbit() {  a = e = i = l = omega = m = 0.0; }

    /** @return A copy of this orbit. **/
    public Orbit copy() {
        return new Orbit(a, e, i, l, omega, m);
    }

    /** Sets the fields of this orbit from the given orbit.
     * @param other A orbit. */
    public void set(final Orbit other) {
        a = other.a;
        e = other.e;
        i = other.i;
        l = other.l;
        omega = other.omega;
        m = other.m;
    }

    /** @return The semi-major axis. **/
    public double getSemimajorAxis() { return a; }

    /** Sets the value of semi-major axis from the given value.
     * @param a A new value of semi-major axis. */
    public void setSemimajorAxis(double a) { this.a = a; }

    /** @return The eccentricity. **/
    public double getEccentricity() { return e; }

    /** Sets the value of eccentricity from the given value.
     * @param e A new value of eccentricity. */
    public void setEccentricity(double e) { this.e = e; }

    /** @return The inclination. **/
    public double getInclination() { return i; }

    /** Sets the value of inclination from the given value.
     * @param i A new value of inclination. */
    public void setInclination(double i) { this.i = i; }

    /** @return The longitude of the ascending node. **/
    public double getLongitude() { return l; }

    /** Sets the longitude of the ascending node from the given value.
     * @param l A new longitude of the ascending node. */
    public void setLongitude(double l) { this.l = l; }

    /** @return The argument of periapsis. **/
    public double getPeriapsisArgument() { return omega; }

    /** Sets the argument of periapsis from the given value.
     * @param omega A new value of argument of periapsis.
     *              It can range from 0 to 360. */
    public void setPeriapsisArgument(double omega) {
        if (omega < 0 || omega > 360) {
            throw new IllegalArgumentException("An argument of " +
                    "periapsis can range from 0 to 360.");
        }
        this.omega = omega;
    }

    /** @return The mean anomaly. **/
    public double getMeanAnomaly() { return m; }

    /** Sets the value of mean anomaly from the given value.
     * @param m A new value of mean anomaly. */
    public void setMeanAnomaly(double m) { this.m = m; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orbit orbit = (Orbit) o;

        if (Double.compare(orbit.a, a) != 0) return false;
        if (Double.compare(orbit.e, e) != 0) return false;
        if (Double.compare(orbit.i, i) != 0) return false;
        if (Double.compare(orbit.l, l) != 0) return false;
        if (Double.compare(orbit.omega, omega) != 0) return false;
        return Double.compare(orbit.m, m) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(a);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(e);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(i);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(l);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(omega);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(m);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Orbit{" +
                "a=" + a +
                ", e=" + e +
                ", i=" + i +
                ", l=" + l +
                ", omega=" + omega +
                ", m=" + m +
                '}';
    }
}
