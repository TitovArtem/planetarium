package com.planetarium.core.physics.axis;

import com.planetarium.core.math.FloatMath;
import com.planetarium.core.objects.SpaceObject;

/**
 * Describes engine for rotating space objects.
 */
public class RotationEngine implements AxisEngine {
    /** The angular speed for this engine. */
    private double angularSpeed;

    /** Creates a new rotation engine for object with the
     * given angular speed.
     * @param angularSpeed The angular speed. */
    public RotationEngine(double angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    /** Creates a new rotation engine for the given space object.
     * @param object A space object for rotating. */
    public RotationEngine(final SpaceObject object) {
        set(object);
    }

    @Override
    public void set(final SpaceObject object) {
        angularSpeed = object.getAxisData().getAngularSpeed();
    }

    @Override
    public void rotate(SpaceObject object, double days) {
        if (!isEqual(object)) {
            set(object);
        }
        AxisData curAxisData = object.getAxisData();
        object.getAxisData().setRotationAngle(
                getNewRotationAngle(curAxisData.getRotationAngle(), days));
    }

    @Override
    public float rotateAt(double days) {
        return (float) (days * angularSpeed);
    }

    @Override
    public AxisEngine copy() {
        return new RotationEngine(angularSpeed);
    }

    private float getNewRotationAngle(float objectAngle, double days) {
        float angle = (float) (days * angularSpeed);
        float resultAngle = objectAngle + angle;
        if (FloatMath.abs(resultAngle) > 360.0) {
            if (resultAngle > 0.0) {
                while (resultAngle > 360.0f)
                    resultAngle -= 360.0f;
            }
            else {
                while (resultAngle < -360.0f)
                    resultAngle += 360.0f;
            }
        }
        return resultAngle;
    }

    private boolean isEqual(final SpaceObject object) {
        double angularSpeedTemp = object.getAxisData().getAngularSpeed();
        return Double.compare(angularSpeedTemp, angularSpeed) == 0;
    }

}
