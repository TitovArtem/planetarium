package com.planetarium.core.physics.axis;

import com.planetarium.core.objects.SpaceObject;

/**
 * Describes a movement of space objects around their axis.
 * It allows to calculate a new rotation angle of space objects.
 */
public interface AxisEngine {

    /** Sets the given space objects for this engine.
     * @param object A space object which will be rotated. */
    void set(SpaceObject object);

    /** Rotates the given space object.
     * @param object A space object which will be rotated.
     * @param value A value describes a time (typically it's solar days). */
    void rotate(SpaceObject object, double value);

    /** Calculates a changing of rotation angle for the given time..
     * @param value A value describes a time (typically it's solar days). */
    float rotateAt(double value);

    /** @return A copy of this axial engine. **/
    AxisEngine copy();
}
