package com.planetarium.core.physics.axis;

import java.io.Serializable;

/**
 * Encapsulates the parameters for moving space object around it's axis
 * and the state of object relatively it's axis.
 * Each object has an axial tilt for describing object's inclination,
 * an angular speed for describing object's rotation speed and a rotation
 * angle for describing object's state relatively it's axis.
 */
public class AxisData implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The axial tilt (in degrees). It's the angle between an object's rotation
     * axis and it's orbital axis. **/
    private float axialTilt;
    /** The angular speed of object which has this axial data.
     * The angular speed of object may be calculated
     * like 360 / orbital period of objects in solar days.
     * The solar day is 24 hours (86164,090530833 seconds ≈ 23 h 56 m 4 s. */
    private double angularSpeed;
    /** The rotation angle. It's angle which describes the rotation of object
     * what has this axial data, relatively it's axis. */
    private float rotationAngle;

    /** Constructs a new axial data.
     * @param axialTilt The value of axial tilt.
     * @param angularSpeed The angular speed of object which has this orbit. */
    public AxisData(float axialTilt, double angularSpeed) {
        this.axialTilt = axialTilt;
        this.angularSpeed = angularSpeed;
        rotationAngle = 0.0f;
    }

    /** Constructs a new axial data.
     * @param axialTilt The value of axial tilt. */
    public AxisData(float axialTilt) {
        this.axialTilt = axialTilt;
        angularSpeed = rotationAngle = 0.0f;
    }

    /** Constructs a new axial data. **/
    public AxisData() {
        axialTilt = rotationAngle = 0.f;
        angularSpeed = 0.0;
    }

    /** @return The axial tilt of this axis. **/
    public float getAxialTilt() {
        return axialTilt;
    }

    /** Sets a new value of the axial tilt.
     * @param axialTilt A new value of axial tilt. */
    public void setAxialTilt(float axialTilt) {
        this.axialTilt = axialTilt;
    }

    /** @return The angular speed. */
    public double getAngularSpeed() { return angularSpeed; }

    /** Sets the angular speed of this axial data from the given speed.
     * @param angularSpeed A new value of angular speed. */
    public void setAngularSpeed(double angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    /** Sets the angular speed of this axial data from the given value of
     * orbital period (in solar days). It calculates like 360 / period.
     * @param period The orbital period of the object. */
    public void setAngularSpeedFromPeriod(double period) {
        if (Double.compare(period, 0.0) == 0) {
            throw new IllegalArgumentException("Value of period has zero value.");
        }
        angularSpeed = 360.0 / period;
    }

    /** @return The rotation angle of space object which
     * has this axial data. **/
    public float getRotationAngle() { return rotationAngle; }

    /** Sets the rotation angle of space object which has this axial data.
     * @param rotationAngle A new value of rotation angle. */
    public void setRotationAngle(float rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    /** Sets fields of this axis like from the given axis.
     * @param other Another axis.  */
    public void set(final AxisData other) {
        axialTilt = other.getAxialTilt();
        angularSpeed = other.getAngularSpeed();
        rotationAngle = other.getRotationAngle();
    }

    /** @return A copy of this axis. **/
    public AxisData copy() {
        AxisData newAxisData = new AxisData();
        newAxisData.set(this);
        return newAxisData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AxisData axisData = (AxisData) o;

        if (Double.compare(axisData.axialTilt, axialTilt) != 0) return false;
        if (Double.compare(axisData.angularSpeed, angularSpeed) != 0) return false;
        return Double.compare(axisData.rotationAngle, rotationAngle) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(axialTilt);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(angularSpeed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rotationAngle);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AxisData{" +
                "axialTilt=" + axialTilt +
                ", angularSpeed=" + angularSpeed +
                ", rotationAngle=" + rotationAngle +
                '}';
    }
}
