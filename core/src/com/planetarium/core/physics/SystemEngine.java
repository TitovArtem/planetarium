package com.planetarium.core.physics;

import com.planetarium.core.objects.SpaceObject;
import com.planetarium.core.objects.SpaceSystem;
import com.planetarium.core.physics.axis.AxisEngine;
import com.planetarium.core.physics.axis.RotationEngine;
import com.planetarium.core.physics.orbit.KeplerOrbitEngine;
import com.planetarium.core.physics.orbit.OrbitEngine;

import java.util.ArrayList;

/**
 * Describes a movement of space objects system.
 * It can use a different object engines to move objects
 * of the given system, but each object have to has one orbital engine
 * and one axis engine. The default object engines for all
 * objects are <KeplerOrbitEngine> and <RotationEngine>.
 * @see KeplerOrbitEngine
 * @see RotationEngine
 */
public class SystemEngine {
    /** The arrays of composites like engines - object in the system. **/
    private ArrayList<Composite> composites;

    /** Constructs a new system engine for the given space system.
     * The default object engines for all objects is <KeplerOrbitEngine>.
     * @see KeplerOrbitEngine
     * @param spaceSystem The space system for movement. */
    public SystemEngine(SpaceSystem spaceSystem) {
        init(spaceSystem);
    }

    /** Sets the orbital engine for each object of this system
     * like the given orbital engine.
     * @param engine The given engine like prototype for all
     *               engines of this system. */
    public void setAllEnginesLike(final OrbitEngine engine) {
        for (int i = 0; i < composites.size(); i++) {
            composites.get(i).orbitEngine = engine.copy();
            composites.get(i).orbitEngine.set(composites.get(i).object);
        }
    }

    /** Initializes engines for each object from the given space system.
     * The default orbital engines for all objects is <KeplerOrbitEngine>.
     * The default axial engines for all objects is <RotationEngine>.
     * @see KeplerOrbitEngine
     * @see RotationEngine
     * @param spaceSystem The system of space objects. */
    public void init(final SpaceSystem spaceSystem) {
        composites = new ArrayList<>(spaceSystem.getSize());
        for (int i = 0; i < spaceSystem.getSize(); i++) {
            SpaceObject object = spaceSystem.getObject(i);
            OrbitEngine orbitEngine = new KeplerOrbitEngine(object);
            AxisEngine axisEngine = new RotationEngine(object);
            composites.add(new Composite(object, orbitEngine, axisEngine));
        }
    }

    // TODO: add access methods

    /** Moves the all objects of this system at new position
     * of their orbit.
     * @param days The number of days. */
    public void move(double days) {
        for (Composite composite : composites) {
            composite.move(days);
        }
    }

    /**
     * Encapsulates a connection of space object with axis and orbit engines.
     */
    private class Composite {
        /** The space object. **/
        private SpaceObject object;
        /** The orbit engine. **/
        private OrbitEngine orbitEngine;
        /** The axis engine. **/
        private AxisEngine axisEngine;

        /** Constructs a new composite of engines and space object. **/
        public Composite(SpaceObject object,
                         OrbitEngine orbitEngine,
                         AxisEngine axisEngine)
        {
            this.object = object;
            this.axisEngine = axisEngine;
            this.orbitEngine = orbitEngine;
        }

        /** Moves the object by the engines. **/
        void move(double days) {
            axisEngine.rotate(object, days);
            orbitEngine.move(object, days);
        }
    }
}
