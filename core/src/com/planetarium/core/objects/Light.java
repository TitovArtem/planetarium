package com.planetarium.core.objects;

import com.planetarium.core.graphics.Color;
import com.planetarium.core.math.Vector3;

/**
 * Describes illuminant objects.
 * It has values of brightness and color to describe it like light
 * and vector of center to know where it is.
 */
public interface Light {
    /** @return The color of this star for changing.. **/
    Color getColor();

    /** Sets the color of this star.
     * @param color A color. */
    void setColor(Color color);

    /** @return The brightness of this star. **/
    double getBrightness();

    /** Sets the value of brightness.
     * @param brightness A new brightness in range [0.0, 1.0]. */
    void setBrightness(double brightness);

    /** @return The center of this light. **/
    Vector3 getCenter();

    /** Sets the center of this light.
     * @param center A new vector of the center. */
    void setCenter(Vector3 center);

    /** Sets the new center of this light.
     * @param x The x-component of a new center.
     * @param y The y-component of a new center.
     * @param z The z-component of a new center. */
    void setCenter(float x, float y, float z);
}
