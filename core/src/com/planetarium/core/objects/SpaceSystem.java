package com.planetarium.core.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes a system of space objects.
 * It can be system of different objects, for example:
 * - A planetary system (The Solar System).
 * - A system of planet and satellites (Earth and Moon).
 * - The systems with comets.
 */
public class SpaceSystem implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The list of the objects. **/
    protected ArrayList<SpaceObject> objects;

    /** Creates a new system from the given space objects.
     * @param objects The space objects. */
    public SpaceSystem(final ArrayList<SpaceObject> objects) {
        this.objects = new ArrayList<>(objects);
    }

    /** Creates a new system from the given space object.
     * @param object The space object. */
    public SpaceSystem(final SpaceObject object) {
        objects = new ArrayList<>();
        objects.add(object);
    }

    /** Gets objects of this system for changing. **/
    public List<SpaceObject> getObjects() { return objects; }

    /** Sets objects of this system from the given ArrayList.
     * @param objects A new objects. */
    public void setObjects(final ArrayList<SpaceObject> objects) {
        this.objects = new ArrayList<>(objects);
    }

    /** Returns the space object at the given position.
     * @param i Index of the element to return.
     * @return The space object at the given position for changing. */
    public SpaceObject getObject(int i) { return objects.get(i); }

    /** Sets the space object at the given position.
     * @param i Index of the element to return.
     * @param object A new object. */
    public void setObject(int i, SpaceObject object) {
        objects.set(i, object);
    }

    /** Adds the space object in the tail of objects array of this system.
     * @param object A new space object. */
    public void add(SpaceObject object) { objects.add(object); }

    /** Removes the object from objects array of this
     * system at the given position.
     * @param i Index of the element to return. */
    public void remove(int i) { objects.remove(i); }

    /** @return Gets number of objects in the system. **/
    public int getSize() { return objects.size(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpaceSystem that = (SpaceSystem) o;

        return !(objects != null ? !objects.equals(that.objects) : that.objects != null);

    }

    @Override
    public int hashCode() {
        return objects != null ? objects.hashCode() : 0;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object obj : objects) {
            stringBuilder.append(obj.toString());
        }
        return stringBuilder.toString();
    }
}
