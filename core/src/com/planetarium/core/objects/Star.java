package com.planetarium.core.objects;

import com.planetarium.core.graphics.Color;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.physics.axis.AxisData;
import com.planetarium.core.physics.orbit.Orbit;

import java.io.Serializable;

/**
 * Describes a stars. All stars has shape like sphere. Each star has a values
 * of brightness and color - it's not physical values which describe
 * a visible effect of luminosity and temperature. */
public class Star extends IlluminantSpaceObject implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Constructs a new star from the given values.
     * @param sphere A sphere.
     * @param brightness A value of brightness in range [0.0, 1.0].
     * @param color A color.  */
    public Star(Sphere sphere, double brightness, Color color) {
        if (sphere == null) {
            throw new NullPointerException("The given sphere has null value.");
        }

        if (brightness < 0 || brightness > 1.0) {
            throw new IllegalArgumentException("A brightness can " +
                    "range from 0.0 to 1.0 ");
        }

        this.brightness = brightness;
        shape = sphere;
        this.color = new Color(color);
        orbit = new Orbit();
        axisData = new AxisData();
    }

    /** Constructs a new star from the given values.
     * @param radius A radius of this star.
     * @param brightness A value of brightness in range [0.0, 1.0].
     * @param color A color.  */
    public Star(float radius, double brightness, Color color) {
        if (brightness < 0 || brightness > 1.0) {
            throw new IllegalArgumentException("A brightness can " +
                    "range from 0.0 to 1.0 ");
        }

        this.brightness = brightness;
        shape = new Sphere(0, 0, 0, radius);
        this.color = new Color(color);
        orbit = new Orbit();
        axisData = new AxisData();
    }

    /** Sets the sphere of this star.
     * @param sphere A new sphere. */
    public void setSphere(final Sphere sphere) {
        shape.setCenter(sphere.getCenter());
        ((Sphere)shape).setRadius(sphere.getRadius());
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Star star = (Star) o;
        if (!star.orbit.equals(orbit)) return false;

        if (Double.compare(star.brightness, brightness) != 0) return false;
        if (shape != null ? !shape.equals(star.shape) : star.shape != null) return false;
        return !(color != null ? !color.equals(star.color) : star.color != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = shape != null ? shape.hashCode() : 0;
        temp = Double.doubleToLongBits(brightness);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
