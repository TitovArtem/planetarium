package com.planetarium.core.objects;

import com.planetarium.core.graphics.Color;
import com.planetarium.core.math.Vector3;

/**
 * Describes a space objects which have property of illuminant.
 * It has values of brightness and color to describe it.
 */
public abstract class IlluminantSpaceObject extends SpaceObject implements Light {
    /** The brightness factor. It can range from 0.0 to 1.0. **/
    protected double brightness;
    /** The color of this star. **/
    protected Color color;

    /** @return The color of this star for changing.. **/
    public Color getColor() { return color; }

    /** Sets the color of this star.
     * @param color A color. */
    public void setColor(Color color) { this.color.set(color); }

    /** @return The brightness of this star. **/
    public double getBrightness() { return brightness; }

    /** Sets the value of brightness.
     * @param brightness A new brightness in range [0.0, 1.0]. */
    public void setBrightness(double brightness) {
        if (brightness < 0 || brightness > 1.0) {
            throw new IllegalArgumentException("A brightness can " +
                    "range from 0.0 to 1.0 ");
        }

        this.brightness = brightness;
    }

    @Override
    public Vector3 getCenter() {
        return shape.getCenter();
    }

    @Override
    public void setCenter(final Vector3 center) {
        super.setCenter(center);
    }

    @Override
    public void setCenter(float x, float y, float z) {
        super.setCenter(x, y, z);
    }
}
