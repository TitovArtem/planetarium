package com.planetarium.core.objects;

import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.geom.Shape3D;
import com.planetarium.core.physics.axis.AxisData;
import com.planetarium.core.physics.orbit.Orbit;

/**
 * Describes all space objects.
 * Each space object has:
 * - orbit to describe a movement of object in a space and
 * to get a position of object relatively a time.
 * - axis data to describe a movement of object relatively it's
 * axis and to get a position of object relatively it's axis.
 * - shape to describe object geometry and position in a space.
 */
public abstract class SpaceObject {
    /** The orbit of this object. **/
    protected Orbit orbit;
    /** The axis of this object. **/
    protected AxisData axisData;
    /** The shape of this object. **/
    protected Shape3D shape;
    /** The information about this object. **/
    protected SpaceObjectInfo info;

    /** @return The axis of this object for changing. **/
    public AxisData getAxisData() { return axisData; }

    /** Sets the axis of this object.
     * @param axisData An axis. */
    public void setAxisData(AxisData axisData) {
        this.axisData.set(axisData);
    }

    /** @return The orbit of this object for changing. **/
    public Orbit getOrbit() { return orbit; }

    /** Sets the orbit of this object.
     * @param orbit A orbit. */
    public void setOrbit(Orbit orbit) {
        this.orbit.set(orbit);
    }

    /** @return A shape that describes this object for changing. **/
    public Shape3D getShape() { return shape; }

    /** Sets the new center of this object.
     * @param center A vector for new center. */
    public void setCenter(final Vector3 center) {
        shape.setCenter(center);
    }

    /** Sets the new center of this object.
     * @param x The x-component of a new center.
     * @param y The y-component of a new center.
     * @param z The z-component of a new center. */
    public void setCenter(float x, float y, float z) {
        shape.setCenter(x, y, z);
    }

    /** @return The information about this object. **/
    public SpaceObjectInfo getInfo() {
        return info;
    }

    /** Sets information about this object.
     * @param info The information for this object. */
    public void setInfo(SpaceObjectInfo info) {
        this.info = info;
    }

 @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpaceObject object = (SpaceObject) o;

        if (orbit != null ? !orbit.equals(object.orbit) : object.orbit != null) return false;
        if (axisData != null ? !axisData.equals(object.axisData) : object.axisData != null) return false;
        return !(shape != null ? !shape.equals(object.shape) : object.shape != null);

    }

    @Override
    public int hashCode() {
        int result = orbit != null ? orbit.hashCode() : 0;
        result = 31 * result + (axisData != null ? axisData.hashCode() : 0);
        result = 31 * result + (shape != null ? shape.hashCode() : 0);
        return result;
    }
}
