package com.planetarium.core.objects;

import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.physics.axis.AxisData;
import com.planetarium.core.physics.orbit.Orbit;

import java.io.Serializable;

/**
 * Describes a planets. Each planet has shape like sphere.
 */
public class Planet extends SpaceObject implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Constructs a new planet.
     * @param sphere The sphere of this planet.
     * @param orbit The Kepler's orbit of this planet.
     * @param axisData The axis data of this planet. */
    public Planet(Sphere sphere, Orbit orbit, AxisData axisData) {
        if (sphere == null) {
            throw new NullPointerException("The given sphere has null value.");
        }

        this.orbit = orbit.copy();
        this.axisData = axisData.copy();
        shape = sphere;
    }

    /** Constructs a new planet.
     * @param radius The radius of this planet.
     * @param orbit The Kepler's orbit of this planet.
     * @param axisData The axis data of this planet. */
    public Planet(float radius, Orbit orbit, AxisData axisData) {
        this.orbit = orbit.copy();
        this.axisData = axisData.copy();
        shape = new Sphere(0, 0, 0, radius);
    }

    /** Sets the sphere of this planet.
     * @param sphere A new sphere. */
    public void setSphere(final Sphere sphere) {
        shape.setCenter(sphere.getCenter());
        ((Sphere)shape).setRadius(sphere.getRadius());
    }

    @Override
    public String toString() {
        return "Planet{" +
                "sphere=" + (Sphere)shape +
                '}';
    }
}
