package com.planetarium.core.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains extra information about space object.
 */
public class SpaceObjectInfo {
    /** The map for the couple field - description. **/
    private Map<String, String> fields = new HashMap<String, String>();

    public void add(String filed, String description) {
        fields.put(filed, description);
    }

    public String get(String field) {
        return fields.get(field);
    }

    public Map get() {
        return fields;
    }
}
