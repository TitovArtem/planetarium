package com.planetarium.core.objects.wireframe;

import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.Vertex;
import com.planetarium.core.math.geom.Triangle;

import java.io.Serializable;

/**
 * Describes a object like a set of vertexes and a set of triangles in 3D.
 */
public class WireframeObject implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The set of triangles of this object. **/
    protected Triangle[] triangles;
    /** The set of vertexes of this object. **/
    protected Vertex[] vertexes;

    /** Constructs a new wire-frame object from the given
     * arrays of triangles and vertexes. It doesn't copy the given arrays,
     * it copies references at the given arrays.
     * @param triangles The array of triangles.
     * @param vertexes The array of vertexes. */
    public WireframeObject(Triangle[] triangles, Vertex[] vertexes) {
        if (triangles == null || vertexes == null) {
            throw new NullPointerException("The given array has null value.");
        }
        this.triangles = triangles;
        this.vertexes = vertexes;
    }

    /** @return The array of triangles of this object for changing. **/
    public Triangle[] getTriangles() { return triangles; }

    /** @return The array of vertexes of this object for changing. **/
    public Vertex[] getVertexes() { return vertexes; }

    /** Gets the triangle at the given position.
     * @param i Index for getting triangle.
     * @return The triangle for changing. */
    public Triangle getTriangle(int i) {
        return triangles[i];
    }

    /** Gets the vertex at the given position.
     * @param i Index for getting vertex.
     * @return The vector for changing. */
    public Vertex getVertex(int i) {
        return vertexes[i];
    }

    /** @return Array of positions of vertexes. **/
    public Vector3[] getPositions() {
        Vector3[] vectors = new Vector3[vertexes.length];
        for (int i = 0; i < vertexes.length; i++) {
            vectors[i] = vertexes[i].getPosition();
        }
        return vectors;
    }

    /** Calculates normals to triangles of this object. **/
    public void calculateNormalsToTriangles() {
        for (int i = 0; i < triangles.length; i++) {
            triangles[i].calculateNormal();
        }
    }

}
