package com.planetarium.core.objects.wireframe;

/**
 * Interface for creating a wire frame objects.
 */
public interface WireframeObjectsFactory {
    /** @return A new wireframe object. **/
    WireframeObject create();
}
