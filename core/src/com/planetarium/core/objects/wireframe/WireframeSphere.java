package com.planetarium.core.objects.wireframe;

import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.Vertex;
import com.planetarium.core.math.geom.Triangle;

/**
 * The class of the wire-frame object with fields and methods
 * to set the essential things to describe a wire-frame sphere.
 * It has fields to describe a position of wire-frame sphere in the
 * space and the methods to calculate normals to vertexes.
 */
public class WireframeSphere extends WireframeObject {
    private static final long serialVersionUID = 1L;

    /** The center of this wire-frame sphere. **/
    private Vector3 center;
    /** The rotation angle of this wire-frame sphere. **/
    private float rotationAngle;

    /* The temporary values of the number of meridians and parallels. */
    private transient int numMeridians, numParallels;

    /** Constructs a new wire-frame sphere from the given
     * arrays of triangles and vertexes. It doesn't copy the given arrays,
     * it copies references at the given arrays.
     * @param triangles The array of triangles.
     * @param vertexes The array of vertexes.
     * @param center The center of sphere.
     * @param rotationAngle The rotation angle of this sphere. */
    public WireframeSphere(Triangle[] triangles, Vertex[] vertexes,
                           final Vector3 center, float rotationAngle) {
        super(triangles, vertexes);
        this.center = center.copy();
        this.rotationAngle = rotationAngle;
    }

    /** Constructs a new wire-frame sphere from the given wire-frame object.
     *  It doesn't copy the arrays of the triangles and the vertexes of
     *  the given wire-frame object, it copies references at that.
     * @param sphere The wire-frame object for sphere.
     * @param center The center of sphere.
     * @param rotationAngle The rotation angle of this sphere. */
    public WireframeSphere(WireframeObject sphere,
                           final Vector3 center,
                           float rotationAngle)
    {
        super(sphere.triangles, sphere.vertexes);
        this.center = center.copy();
        this.rotationAngle = rotationAngle;
    }

    /** @return The center of this wire-frame sphere for changing. **/
    public Vector3 getCenter() {
        return center;
    }

    /** Sets a new center of this wire-frame sphere.
     * @param center The new center for this wire-frame sphere. */
    public void setCenter(Vector3 center) {
        this.center = center;
    }

    /** @return The rotation angle of this wire-frame sphere. **/
    public float getRotationAngle() {
        return rotationAngle;
    }

    /** Sets a new value of the rotation angle of this wire-frame sphere.
     * @param rotationAngle A new rotation angle for this wire-frame sphere. */
    public void setRotationAngle(float rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    /** Calculates the normals to vertexes of this wire-frame sphere.
     * Before calculating normals to vertexes it calculates normals to
     * triangles of this wire-frame model.
     * If numMeridians * numParallels isn't equal to number of vertexes of
     * this wire-frame sphere, then you will get <IllegalArgumentException>.
     * @param numMeridians The number of meridians for this wire-frame sphere.
     * @param numParallels The number of parallels for this wire-frame sphere. */
    public void calculateNormals(int numMeridians, int numParallels) {
        if (numMeridians * numParallels != vertexes.length) {
            throw new IllegalArgumentException("The number of meridians and " +
                    "parallels must be the same with number of vertexes. ");
        }
        this.numMeridians = numMeridians;
        this.numParallels = numParallels;
        calculateNormalsToTriangles();
        calculateNormalsToVertexes(triangles);
    }

    private int getIndex(int val) {
        val = (val < 0) ? (numMeridians - 1) * 2 + val : val;
        val = (val >= (numMeridians - 1) * 2) ? val - (numMeridians - 1) * 2 : val;
        return val;
    }

    /** Calculates and sets normal to point a. Look at the comment for triangulate(). **/
    private void setNormalToA(Triangle[] triangles, int i, int j) {
        int prefix = (numMeridians - 1) * 2;
        Vector3 n1 = triangles[(i-1) * prefix + getIndex(j-1)].getNormal();
        Vector3 n2 = triangles[(i-1) * prefix + j].getNormal();
        Vector3 n3 = triangles[(i-1) * prefix + j+1].getNormal();
        Vector3 n4 = triangles[i * prefix + getIndex(j-2)].getNormal();
        Vector3 n5 = triangles[i * prefix + getIndex(j-1)].getNormal();
        Vector3 n6 = triangles[i * prefix + j].getNormal();

        Vector3 result = n1.copy().add(n2).add(n3).add(n4).add(n5).add(n6);
        result.normalize();
        triangles[i * prefix + j].getA().setNormal(result);
    }


    /** Calculates and sets normal to point b. Look at the comment for triangulate(). **/
    private void setNormalToB(Triangle[] triangles, int i, int j) {
        int prefix = (numMeridians - 1) * 2;
        Vector3 n1 = triangles[(i-1) * prefix + j+1].getNormal();
        Vector3 n2 = triangles[(i-1) * prefix + getIndex(j+2)].getNormal();
        Vector3 n3 = triangles[(i-1) * prefix + getIndex(j+3)].getNormal();
        Vector3 n4 = triangles[i * prefix + j].getNormal();
        Vector3 n5 = triangles[i * prefix + getIndex(j+1)].getNormal();
        Vector3 n6 = triangles[i * prefix + getIndex(j+2)].getNormal();

        Vector3 result = n1.copy().add(n2).add(n3).add(n4).add(n5).add(n6);
        result.normalize();
        triangles[i * prefix + j].getC().setNormal(result);
    }

    /** Calculates normals to each vertex. **/
    private void calculateNormalsToVertexes(Triangle[] triangles) {
        for (int i = 1; i < numParallels - 1; i++) {
            for (int j = 0; j < (numMeridians - 1) * 2; j += 4) {
                setNormalToA(triangles, i, j);
                setNormalToB(triangles, i, j);
            }
        }

        // Calculate normals to vertexes at the top poles
        Vector3 topPoleNormal = new Vector3();
        Vector3 bottomPoleNormal = new Vector3();
        int bottomIndex = (numMeridians - 1) * 2 * (numParallels - 2);

        for (int i = 0; i < (numMeridians - 1) * 2; i++) {
            topPoleNormal.add(triangles[i].getNormal());
            bottomPoleNormal.add(triangles[bottomIndex + i].getNormal());
        }

        topPoleNormal.normalize();
        bottomPoleNormal.normalize();

        for (int i = 0; i < (numMeridians - 1) * 2; i++) {
            if (i % 2 == 0) {
                triangles[i].getA().setNormal(topPoleNormal);
                triangles[i].getC().setNormal(topPoleNormal);
            } else {
                triangles[bottomIndex + i].getA().setNormal(topPoleNormal);
                triangles[bottomIndex + i].getB().setNormal(topPoleNormal);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WireframeSphere that = (WireframeSphere) o;

        if (Float.compare(that.rotationAngle, rotationAngle) != 0) return false;
        if (numMeridians != that.numMeridians) return false;
        if (numParallels != that.numParallels) return false;
        return !(center != null ? !center.equals(that.center) : that.center != null);

    }

    @Override
    public int hashCode() {
        int result = center != null ? center.hashCode() : 0;
        result = 31 * result + (rotationAngle != +0.0f ? Float.floatToIntBits(rotationAngle) : 0);
        result = 31 * result + numMeridians;
        result = 31 * result + numParallels;
        return result;
    }
}
