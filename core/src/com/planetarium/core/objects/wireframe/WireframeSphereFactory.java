package com.planetarium.core.objects.wireframe;

import com.planetarium.core.graphics.Dimension;
import com.planetarium.core.math.FloatMath;
import com.planetarium.core.math.Vector2i;
import com.planetarium.core.math.Vector3;
import com.planetarium.core.math.Vertex;
import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.math.geom.Triangle;

import java.io.Serializable;
import java.util.ArrayList;

import static com.planetarium.core.math.FloatMath.*;

/**
 * Describes the factory for creating wire-frame spheres.
 * It creates the wire-frame model of sphere like set of meridians
 * and parallels. By default, the number of meridians and parallels is 16.
 */
public class WireframeSphereFactory implements WireframeObjectsFactory, Serializable {
    private static final long serialVersionUID = 1L;

    /** The prototype for creating wire-frame spheres. **/
    private Sphere sphere;
    /** The number of meridians for wire-frame spheres. **/
    private int numMeridians = 16;
    /** The number of parallels for wire-frame spheres. **/
    private int numParallels = 16;
    
    /** The dimension of the texture. **/
    private Dimension textureDimension = null;

    /** Creates a new factory. This factory will not calculate normals
     * and texture coordinates for each vertex.
     * @param prototype A prototype of sphere. */
    public WireframeSphereFactory(final Sphere prototype) {
        sphere = prototype.copy();
    }

    /** Creates a new factory.
     * @param prototype A prototype of sphere.
     * @param dimension The dimension of texture to calculate the texture
     *                  coordinates. If it's null - factory will not calculate
     *                  texture coordinates. */
    public WireframeSphereFactory(final Sphere prototype,
                                  final Dimension dimension)
    {
        sphere = prototype.copy();
        if (dimension != null) {
            textureDimension = new Dimension(dimension);
        }
    }

    /** @return The prototype of the sphere for changing. **/
    public Sphere getSphere() { return sphere; }

    /** Sets the prototype of the sphere.
     * @param prototype A sphere-prototype. */
    public void setSphere(final Sphere prototype) {
        this.sphere = prototype.copy();
    }

    /** @return The number of meridians. **/
    public int getNumMeridians() { return numMeridians; }

    /** Sets the number of meridians for wire-frame spheres.
     * @param numMeridians A number of meridians. */
    public void setNumMeridians(int numMeridians) {
        if (numMeridians <= 0) {
            throw new IllegalArgumentException("The number of meridians " +
                    "must be positive. ");
        }
        this.numMeridians = numMeridians;
    }

    /** @return The number of parallels. **/
    public int getNumParallels() { return numParallels; }

    /** Sets the number of parallels for wire-frame spheres.
     * @param numParallels A number of parallels. */
    public void setNumParallels(int numParallels) {
        if (numParallels <= 0) {
            throw new IllegalArgumentException("The number of parallels " +
                    "must be positive.");
        }
        this.numParallels = numParallels;
    }

    @Override
    public WireframeObject create() {
        Vertex[][] meridians = constructMeridians();
        Vertex[] vertexes = getVertexesFromMeridians(meridians);
        Triangle[] triangles = triangulate(meridians);
        return new WireframeObject(triangles, vertexes);
    }

    /** Calculate texture coordinates for the given vertex. **/
    private void calculateTexel(Vertex vertex) {
        Vector3 pos = vertex.getPosition().copy().sub(sphere.getCenter());
        pos.normalize();
        float u = 0.5f + FloatMath.atan2(pos.y, pos.x) / FloatMath.DOUBLE_PI;
        float v = 0.5f - FloatMath.asin(pos.z) / FloatMath.PI;

        vertex.setTexel(new Vector2i(u * (textureDimension.getWidth() - 1),
                v * (textureDimension.getHeight() - 1)));
    }

    /** Creates the array of meridians of the sphere. **/
    private Vertex[][] constructMeridians() {
        Vertex[][] sphereData = new Vertex[numParallels][numMeridians];
        float dl = 2 * PI / (numMeridians - 1);
        float smoothStep = PI / (numParallels - 1);
        float radius = sphere.getRadius();
        int i = 0;
        for (float l = 0f; l <= 2 * PI + dl / 2; l += dl, i++) {
            int j = 0;
            for (float b = -PI / 2; b <= PI / 2 + smoothStep / 2; b += smoothStep, j++) {
                float x =  radius * cos(b) * sin(l) + sphere.getCenter().x;
                float y = radius * cos(b) * cos(l) + sphere.getCenter().y;
                float z = radius * sin(b) + sphere.getCenter().z;
                sphereData[j][i] = new Vertex(new Vector3(x, y, z));

                if (textureDimension != null) {
                    calculateTexel(sphereData[j][i]);
                }
            }
        }
        return sphereData;
    }


    /** Gets array of triangles for wire-frame sphere.
     *   a *---* b
     *     |   |
     *   d *---* c
     **/
    private Triangle[] triangulate(Vertex[][] meridians) {
        ArrayList<Triangle> triangles = new ArrayList<>();
        for (int i = 0; i < numParallels - 1; i++) {
            for (int j = 0; j < numMeridians - 1; j++) {
                Vertex a = meridians[i][j];
                Vertex b = meridians[i + 1][j];
                Vertex c = meridians[i + 1][j + 1];
                Vertex d = meridians[i][j + 1];

                Triangle first = new Triangle(a, b, d);
                Triangle second = new Triangle(b, c, d);
                triangles.add(first);
                triangles.add(second);
            }
        }
        return triangles.toArray(new Triangle[triangles.size()]);
    }

    /** Transform matrix of meridians to array of vertexes. **/
    private Vertex[] getVertexesFromMeridians(Vertex[][] meridians) {
        Vertex[] vertexes = new Vertex[numMeridians * numParallels];
        for (int i = 0; i < numParallels; i++) {
            for (int j = 0; j < numMeridians; j++) {
                vertexes[i * numMeridians + j] = meridians[i][j];
            }
        }
        return vertexes;
    }
}
