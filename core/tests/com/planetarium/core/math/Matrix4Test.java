package com.planetarium.core.math;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test {@link com.planetarium.core.math.Matrix4}
 */
public class Matrix4Test {

    private Matrix4 matrix1234;
    private Matrix4 zero;
    private Matrix4 temp;

    private final double EPS = 0.000001;

    @Before
    public void setUp() throws Exception {
        float data[][] = {
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4} };
        matrix1234 = new Matrix4(data);
        zero = Matrix4.ZEROS;
        float data1[][] = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16} };
        temp = new Matrix4(data1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGet() throws Exception {
        zero.get(-5, -5);
        zero.get(0, -5);
        zero.get(-5, 0);
        zero.get(4, 4);
        zero.get(2, 4);
        zero.get(4, 2);
    }

    @Test
    public void testGet2() {
        assertEquals(2, matrix1234.get(1, 1), EPS);
        assertEquals(4, matrix1234.get(0, 3), EPS);
    }

    @Test(expected = NullPointerException.class)
    public void testSet() throws Exception {
        float data[][] = null;
        zero.set(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSet1() throws Exception {
        float data1[][] = new float[1][5];
        zero.set(data1);
        float data2[][] = new float[4][5];
        zero.set(data2);
        float data3[][] = new float[4][4];
        zero.set(data3);
    }

    @Test
    public void testSetColumn() throws Exception {
        float arr[] = {100, 100, 100, 100};
        matrix1234.setColumn(1, arr);
        assertEquals(100, matrix1234.get(0, 1), EPS);
        assertEquals(100, matrix1234.get(1, 1), EPS);
        assertEquals(100, matrix1234.get(2, 1), EPS);
        assertEquals(100, matrix1234.get(3, 1), EPS);
    }

    @Test
    public void testMultiple() throws Exception {
        matrix1234.multiple(temp);

        float arr0[] = {90, 100, 110, 120};
        assertArrayEquals(matrix1234.getRow(0), arr0, (float) EPS);
        float arr1[] = {90, 100, 110, 120};
        assertArrayEquals(matrix1234.getRow(1), arr1, (float)EPS);
        float arr2[] = {90, 100, 110, 120};
        assertArrayEquals(matrix1234.getRow(2), arr2, (float)EPS);
        float arr3[] = {90, 100, 110, 120};
        assertArrayEquals(matrix1234.getRow(3), arr3, (float) EPS);
    }

    @Test
    public void testTranspose() throws Exception {
        temp.transpose();

        float arr0[] = {1, 5, 9, 13};
        assertArrayEquals(temp.getRow(0), arr0, (float) EPS);
        float arr1[] = {2, 6, 10, 14};
        assertArrayEquals(temp.getRow(1), arr1, (float)EPS);
        float arr2[] = {3, 7, 11, 15};
        assertArrayEquals(temp.getRow(2), arr2, (float)EPS);
        float arr3[] = {4, 8, 12, 16};
        assertArrayEquals(temp.getRow(3), arr3, (float) EPS);
    }
}