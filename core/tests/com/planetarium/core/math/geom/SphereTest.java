package com.planetarium.core.math.geom;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test class of Sphere {@link com.planetarium.core.math.geom.Sphere}
 */
public class SphereTest {

    @Test
    public void extendsTest() {
        Shape3D shape = new Sphere(0, 0, 0, 50);
        System.out.println(shape);
        Sphere sphere = new Sphere(0, 0, 0, 50);
        System.out.println(sphere);
    }

}