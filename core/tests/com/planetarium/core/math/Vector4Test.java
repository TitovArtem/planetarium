package com.planetarium.core.math;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test {@link com.planetarium.core.math.Vector4}.
 */
public class Vector4Test {

    final double EPS = 0.000001;

    private Vector4 vector1, vector2;

    @Before
    public void setUp() {
        vector1 = new Vector4(1, 2, 3, 4);
        vector2 = new Vector4(10, 20, 30, 40);
    }

    @Test
    public void testCopy() throws Exception {
        Vector4 before = new Vector4(1, 2, 3, 4);
        Vector4 after = before.copy();
        assertEquals(before.x, after.x, EPS);
        after.x = -50;
        assertNotEquals(before.x, after.x, EPS);
    }

    @Test
    public void testMultipleVectorAtMatrix() throws Exception {
        float data[][] = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16} };
        Matrix4 matrix = new Matrix4(data);
        vector1.multiple(matrix);
        assertEquals(vector1.x, 90, EPS);
        assertEquals(vector1.y, 100, EPS);
        assertEquals(vector1.z, 110, EPS);
        assertEquals(vector1.w, 120, EPS);
    }

    @Test
    public void testMultipleMatrixAtVector() throws Exception {
        float data[][] = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16} };
        Matrix4 matrix = new Matrix4(data);
        Vector4 col = Vector4.multiple(matrix, vector1);
        assertEquals(col.x, 30, EPS);
        assertEquals(col.y, 70, EPS);
        assertEquals(col.z, 110, EPS);
        assertEquals(col.w, 150, EPS);
    }

    @Test
    public void testIsCollinear() throws Exception {
        assertTrue(vector1.isCollinear(vector2));
        vector1.x = -35;
        assertFalse(vector1.isCollinear(vector2));
    }
}