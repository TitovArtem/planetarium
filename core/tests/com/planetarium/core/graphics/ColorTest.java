package com.planetarium.core.graphics;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The test of {@link com.planetarium.core.graphics.Color}
 */
public class ColorTest {

    private Color first;

    @Before
    public void setUp() throws Exception {
        first = new Color(255, 255, 255);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSet() throws Exception {
        first.set(-20, 50, 50, 50);
        first.set(50, -20, 50, 50);
        first.set(50, 50, -20, 50);
        first.set(50, 50, 50, -20);
        first.set(0, 0, 0, 0);
        first.set(256, 0, 0, 0);
        first.set(0, 256, 0, 0);
        first.set(0, 0, 256, 0);
        first.set(0, 0, 0, 256);
    }
}