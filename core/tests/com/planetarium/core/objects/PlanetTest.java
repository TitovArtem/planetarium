package com.planetarium.core.objects;

import com.planetarium.core.math.geom.Sphere;
import com.planetarium.core.physics.axis.AxisData;
import com.planetarium.core.physics.orbit.Orbit;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * The test of {@link com.planetarium.core.objects.Planet}
 */
public class PlanetTest {
    private SpaceObject spaceObject;
    private Planet planet;

    @Before
    public void setUp() throws Exception {
        planet = new Planet(
                new Sphere(0, 0, 0, 50),
                new Orbit(1, 2, 3, 4, 5, 6),
                new AxisData());
        spaceObject = planet;
    }

    @Test
    public void testShapeChanging() throws Exception {
        Sphere sphere = (Sphere)spaceObject.getShape();
        sphere.setRadius(150);
        assertEquals(((Sphere)planet.getShape()).getRadius(), 150, 0.00001);
        setUp();
        sphere = (Sphere)planet.getShape();
        sphere.setRadius(150);
        assertEquals(((Sphere) spaceObject.getShape()).getRadius(), 150, 0.00001);

    }
}